/*
 * Author: Brendan Le Foll <brendan.le.foll@intel.com>
 * Copyright (c) 2014 Intel Corporation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "38z-types.h"
#include "types.h"
#include "mraa/gpio.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include <limits.h>
#include <assert.h>
#include "spi.h"
#include "gpio.h"


//
//	Defines
//
#define ENABLE_ASSERTS					(1)
#define SPI_FREQ_420KHZ					(420000)
#define SPI_FREQ_200KHZ					(200000)
#define SPI_FREQ_1MHZ					(1000000)
#define SPI_FREQ_2MHZ					(2000000)
#define SPI_FREQ_1p8MHZ					(1800000)
#define DELAY_200_MS					(200 * 1000)
#define PAGE_SELECT_PREFIX				(0x80)
#define READ_REGISTER_PREFIX			(0x00)
#define WRITE_REGISTER_PREFIX			(0x40)
#define INSTRUCTION_PREFIX				(0xC0)
#define EDISON_SPI_SSP5					(5)			// spidev5.x
#define CS5484_CS_PIN_0					(0)			// spidev5.0
#define CS5484_CS_PIN_1					(1)			// spidev5.1

// Instructions can be controls or calibrations
typedef enum {
	ctlSwReset = 1,
	ctlStandby = 2,
	ctlWakeup = 3,
	ctlSingleConv = 0x14,		// 0b010100
	ctlContinuousConv = 0x15,	// 0b010101,
	ctlHaltConv = 0x18,			// 0b011000
} INSTRUCTION_T;

// Page 0 register addresses
enum {
	regConfig0	=	0
};

//
//	Static Data
//
static spi_context spi0;
static spi_context spi1;

static volatile struct timespec lt0, lt1;
static volatile int ctr0 = 0, ctr1 = 0;
static volatile int min = INT_MAX, max = 0;
//
//	Static Functions
//
static void CS5484_Init(spi_mode_t spi_mode_val);
static void CS5484_SetPage(spi_context spi, u8 page);
static u32  CS5484_ReadRegister(spi_context spi, u8 reg);
static void CS5484_WriteRegister(spi_context spi, u8 reg, u32 value);
static void CS5484_WriteInstruction(spi_context spi, INSTRUCTION_T instruction);
static void CS5484_TurnOnCpuClk(spi_context spi);
static int main_loop(void);
static void isr0(void *arg);
static void isr1(void *arg);
static int test_irq(void);

#if ENABLE_ASSERTS
#define ASSERT(x)	assert(x)
#else
#define ASSERT(x)
#endif

int ledstate = 0;
mraa_gpio_context gpio;

int main(int argc, char **argv)
{
#if 1
	// Sanity check blinky LED
//    mraa_gpio_context gpio;
//    int ledstate = 0;


    gpio = mraa_gpio_init_raw(183);
    mraa_gpio_dir(gpio, MRAA_GPIO_OUT);
//    for (;;) {
//        ledstate = !ledstate;
//        mraa_gpio_write(gpio, !ledstate);
//        sleep(1);
//    }
#endif

    main_loop();

    return 0;
}

//
//	Notes:
//
// There are two CS5484 chips on the same SPI bus, but on different chip selects.
// CS5484-U4 is connected to CS1
// CS5484-U5 is connected to CS0


static int main_loop(void)
{
	u32 readVal;
	int i;

	CS5484_Init(SPI_MODE3);

	CS5484_TurnOnCpuClk(spi0);
	CS5484_WriteInstruction(spi0, ctlWakeup);
	CS5484_WriteInstruction(spi0, ctlContinuousConv);

	CS5484_TurnOnCpuClk(spi1);
	CS5484_WriteInstruction(spi1, ctlWakeup);
	CS5484_WriteInstruction(spi1, ctlContinuousConv);

	// Just do a sanity check and read some parms from both CS5484s
	i = 10;
	while(i--)
	{

		CS5484_SetPage(spi0, 16);
		readVal = CS5484_ReadRegister(spi0, 3);
		printf("spi0 Reg 3 read = 0x%x\n", readVal);
		usleep(1000);
		readVal = CS5484_ReadRegister(spi0, 9);
		printf("spi0 Reg 9 read = 0x%x\n", readVal);
		usleep(1000);

		readVal = CS5484_ReadRegister(spi1, 3);
		printf("spi1 Reg 3 read = 0x%x\n", readVal);
		usleep(1000);
		readVal = CS5484_ReadRegister(spi1, 9);
		printf("spi1 Reg 9 read = 0x%x\n", readVal);
		usleep(1000);
	}

	test_irq();

	return 0;
}

// Init both spi busses
static void CS5484_Init(spi_mode_t spi_mode_val)
{
	spi1 = spi_init_raw(EDISON_SPI_SSP5, CS5484_CS_PIN_1);
	spi0 = spi_init_raw(EDISON_SPI_SSP5, CS5484_CS_PIN_0);
	if (!spi0 || !spi1) {
		printf("Couldn't init SPI\n");
		return;
	}
	spi_frequency(spi0, SPI_FREQ_2MHZ);
	spi_frequency(spi1, SPI_FREQ_2MHZ);
	spi_mode(spi0, spi_mode_val);
	spi_mode(spi1, spi_mode_val);
}

static int test_irq(void)
{
	mraa_init();
	mraa_gpio_context x0 = NULL, x1 = NULL;

	x0 = mraa_gpio_init(55);
	if (x0 == NULL)
	{
		goto stop;
	}

	x1 = mraa_gpio_init(52);
	if (x1 == NULL)
	{
		goto stop;
	}

	nice(-20);

	CS5484_SetPage(spi0, 0);
	CS5484_WriteRegister(spi0, 0, 4);
	CS5484_WriteRegister(spi0, 1, 0xBBBB);
	CS5484_SetPage(spi0, 16);
	CS5484_WriteRegister(spi0, 0, 2);
	CS5484_SetPage(spi0, 18);
	CS5484_WriteRegister(spi0, 58, 0x080000);

	CS5484_SetPage(spi1, 0);
	CS5484_WriteRegister(spi1, 0, 4);
	CS5484_WriteRegister(spi1, 1, 0xBBBB);
	CS5484_SetPage(spi1, 16);
	CS5484_WriteRegister(spi1, 0, 2);
	CS5484_SetPage(spi1, 18);
	CS5484_WriteRegister(spi1, 58, 0x080000);

	if (mraa_gpio_dir(x0, MRAA_GPIO_IN) != MRAA_SUCCESS)
	{
		goto stop;
	}

	if (mraa_gpio_dir(x1, MRAA_GPIO_IN) != MRAA_SUCCESS)
	{
		goto stop;
	}

	lt0.tv_sec = lt0.tv_nsec = lt1.tv_sec = lt1.tv_nsec = 0;

	// Note: I think there's a bug in mraa where it doesn't check the return value of pthread_create
	if (mraa_gpio_isr(x0, MRAA_GPIO_EDGE_RISING, isr0, NULL) != MRAA_SUCCESS) {
		goto stop;
	}
	if (mraa_gpio_isr(x1, MRAA_GPIO_EDGE_RISING, isr1, NULL) != MRAA_SUCCESS) {
		goto stop;
	}

	while (1) {
		sleep(1000);
//		printf("%d %d MM %d to %d\n", ctr0, ctr1, min, max);
//		min = INT_MAX;
//		max = 0;
	}

stop:
	printf("Stop has be hit.\n");
	fputs("failed.\n", stderr);
	if (x0 != NULL) {
		mraa_gpio_close(x0);
	}
	if (x1 != NULL) {
		mraa_gpio_close(x1);
	}
	return 1;
}


void isr0(void *arg)
{

	/*
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
	long delta = (ts.tv_sec * 1000 * 1000 + ts.tv_nsec / 1000) - (lt0.tv_sec * 1000 * 1000 + lt0.tv_nsec / 1000);
	if (delta < min) {
		min = delta;
	}
	if (delta > max) {
		max = delta;
	}
	if (delta > 12000) {
		printf("isr0 gap %ld\n\r", delta);
	}
	lt0 = ts;
	ctr0++;
	*/
//	        ledstate = !ledstate;
//	        mraa_gpio_write(gpio, !ledstate);
}

void isr1(void *arg)
{
//	struct timespec ts;
//	clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
//	long delta = (ts.tv_sec * 1000 * 1000 + ts.tv_nsec / 1000) - (lt1.tv_sec * 1000 * 1000 + lt1.tv_nsec / 1000);
//	if (delta > 12000) {
//		printf("isr1 gap %ld\n\r", delta);
//	}
//	lt1 = ts;
//	ctr1++;
    ledstate = !ledstate;
    mraa_gpio_write(gpio, !ledstate);
}

static void CS5484_TurnOnCpuClk(spi_context spi)
{
	u32 writeVal = 0x400000 + (1<<20); // Default + CUCLK_ON
	CS5484_WriteRegister(spi, regConfig0, writeVal);
}

static void CS5484_SetPage(spi_context spi, u8 page)
{
	uint8_t txbuf[] =
	{
		page + PAGE_SELECT_PREFIX,
	};
	uint8_t rxbuf[] =
	{
		0x00,
	};
	spi_transfer_buf(spi, txbuf, rxbuf, 1);
}


static u32 CS5484_ReadRegister(spi_context spi, u8 reg)
{
	u32 returnVal;
	u8 txbuf[] =
	{
			reg | READ_REGISTER_PREFIX,
			0xff,
			0xff,
			0xff,
	};
	u8 rxbuf[] =
	{
			0x00,
			0x00,	// msb
			0x00,	// middle
			0x00,	// lsb
	};

	spi_transfer_buf(spi, txbuf, rxbuf, 4);
	returnVal = ((u32) rxbuf[1] << 16) +
				((u32) rxbuf[2] << 8)  +
				((u32) rxbuf[3]);

	return returnVal;
}

static void CS5484_WriteRegister(spi_context spi, u8 reg, u32 value)
{
	u8 msb = (value & 0xff0000) >> 16;
	u8 second = (value & 0x00ff00) >> 8;
	u8 lsb = (value & 0xff);

	uint8_t txbuf[] =
	{
			reg | WRITE_REGISTER_PREFIX,
			msb,
			second,
			lsb
	};
	uint8_t rxbuf[] =
	{
			0x00,
			0x00,
			0x00,
			0x00
	};
	spi_transfer_buf(spi, txbuf, rxbuf, 4);
}

static void CS5484_WriteInstruction(spi_context spi, INSTRUCTION_T instruction)
{
	uint8_t txbuf[] =
	{
			(u8) instruction | (u8) INSTRUCTION_PREFIX
	};
	uint8_t rxbuf[] =
	{
			0x00
	};
	spi_transfer_buf(spi, txbuf, rxbuf, 1);
}
