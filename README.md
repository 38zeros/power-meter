# README #

### C application(s) for Bosch Power Meter Project ###

### Building & Flashing An Image ###

* To build image for flashing, from the edison-src\ directory. (If anything RED appears, there is a problem with the Yocto build):

```
#!bash
$ source poky/oe-init-build-env && bitbake edison-image -v && cd .. && device-software/utils/flash/postBuild.sh
```
* (NOTE: THIS (and the Intel instructions online that also talk about this) DOES NOT WORK ON MAC OS X! -- Use Ubuntu) To flash to Edison, make sure you are connected to the OTG USB port. from the edison-src\build\toFlash\ directory:
```
#!bash
$ sudo ./flashall.sh
```
	NOTE: You may also need gnu-getopt, coreutils, and dfu-util:
		brew update
		brew install gnu-getopt coreutils dfu-util

* Reboot board and flashing should start.
```
#!bash
$ reboot ota
```

### Quick Start ###

* Config system (done only once after flashing new image):
```
#!bash
$ /etc/38z/install_config.sh
```

<then reconnect the usb hub and then power cycle entire system>

* To configure SSH (done only once after flashing new image):
```
#!bash
$ configure_edison --setup
```
(Set password to 'password')

* If wifi has been set up on the Edison, go into /etc/wpa_supplicant/wpa_supplicant.conf and remove all access point entries, otherwise routing the packets through the GPRS modem becomes harder.

* Create link through GPRS modem by:
```
#!bash
$ ~/connect-to-internet.sh
```

* Wait until ppp0 shows up. Press Control-C to exit connect-to-internet.sh.
* Just in case this fails, you can try to reconnect using:
```
#!bash
$ /etc/ppp/ppp-start
```
* If wifi is connected (you didn't clear out wpa_supplicant.conf), you can redirect the packets by first making sure the file /etc/resolv.conf has only one entry:
```
#!bash
nameserver 8.8.8.8
```
* To redirect the packets from the wlan0 interface to the ppp0 interface, we need to delete the default route going through wlan0 and add a new route that sends packets to ppp0. First we have to find the ip address of wlan0 route by:
```
#!bash
$ route
```
* Switch the routing by:
```
#!bash
$ route del default gw <ip address of default route> wlan0
$ route add default gw <ip address of ppp0>
```

## Who do I talk to? ###

* Rob Baltzer rob.baltzer01@gmail.com

### Useful Links ###

* http://www.emutexlabs.com/project/215-intel-edison-gpio-pin-multiplexing-guide