/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <stdio.h>
#include <linux/types.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <sys/time.h>
#include "types.h"
#include "gpio.h"

#define NA	(0)	// Not Applicable

// Digital GPIO table
static GpioDesc gpioDesc[] = {
//		   GPIO					Direction		    Initial Value
		{ GPIO14, 				DIRECTION_INPUT, 		NA},
};
static const u8 gpioDescSz = sizeof(gpioDesc)/sizeof(GpioDesc);

// Initialize all of the digital gpio
void gpio_init_gpio(void)
{
	u8 i;

	for (i = 0 ; i < gpioDescSz ; i++) {
		gpio_export(gpioDesc[i].pin);
		gpio_set_direction(gpioDesc[i].pin, gpioDesc[i].direction);
		if (gpioDesc[i].direction == DIRECTION_OUTPUT) {
			gpio_set(gpioDesc[i].pin, gpioDesc[i].initial_value);
		}
	}
}

void gpio_export(u32 gpio)
{
	int fd;
	char buf[128];

	fd = open("/sys/class/gpio/export", O_WRONLY);
	sprintf(buf, "%d", gpio);
	write(fd, buf, strlen(buf));
	close(fd);
}

void gpio_set_direction(u32 gpio, GpioDirection direction)
{
	int fd;
	char buf[128];

	sprintf(buf, "/sys/class/gpio/gpio%d/direction", gpio);
	fd = open(buf, O_WRONLY);
	if (direction == DIRECTION_OUTPUT) {
		write(fd, "out", 3);
	}
	else {
		write(fd, "in", 2);
	}
	close(fd);
}

void gpio_set(u32 gpio, bool value)
{
	int fd;
	char buf[128];

	sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
	fd = open(buf, O_WRONLY);
	if (value == 0) {
		write(fd, "0", 1);
	}
	else {
		write(fd, "1", 1);
	}
	close(fd);
}

bool gpio_get(u32 gpio)
{
	int fd;
	char buf[128];
	u8 value;
	struct timeval start, end;
	long mtime, seconds, useconds;
	long j = 3200000;

	sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
	fd = open(buf, O_RDONLY);
    gettimeofday(&start, NULL);
	while(j--) {
		read(fd, &value, 1);
	}
	gettimeofday(&end, NULL);
	close(fd);
	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
	printf("Elapsed time: %ld milliseconds\n", mtime);
	return (value == '0' ? false : true);
}
