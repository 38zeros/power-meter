/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

//
//	Includes
//
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdbool.h>
#include "spi.h"
#include "at90e_xx.h"
#include "gpio.h"

//
//	Defines
//
#define ENABLE_ASSERTS					(1)
#define ATM90_MSB_READ					(0x80)
#define ATM90_MSB_WRITE					(0x00)
#define EDISON_SPI_SSP5					(5)			// spidev5.1
#define ATM90_CS_PIN_1					(1)			// "
#define SPI_FREQ_420KHZ					(420000)
#define SPI_FREQ_1MHZ					(1000000)
#define DELAY_200_MS					(200 * 1000)
#define MAGIC_REGISTER_ACCESS_NUMBER	(0x55AA)
#define MAGIC_SOFTWARE_RESET_NUMBER		(0x789A)
//
//	Static Data
//
static spi_context spi;

//
//	Static Functions
//
static u32 		ATM90_ReadRegister(u16 register_addr);
static void 	ATM90_WriteRegister(u16 register_addr, u16 value);
static void 	ATM90_Init(void);
static u32		ATM90_ReadMeasureParameter(u16 regh_addr, u16 regl_addr);
static double 	ATM90_GetRMSVoltage(V_RMS_TYPE phase);
static double 	ATM90_GetRMSCurrent(I_RMS_TYPE phase);
static void 	ATM90_EnableSPIWrites(void);
static void 	ATM90_DisableSPIWrites(void);
static void 	ATM90_CalCurrentPhaseAOffset(void);
static void 	ATM90_SoftwareReset(void);

#if ENABLE_ASSERTS
#define ASSERT(x)	assert(x)
#else
#define ASSERT(x)
#endif

/***************** MAIN ***********************/
int main(int argc, char *argv[])
{
#if 1
	double Ua, Ub, Uc;
	double Ia, Ib, Ic;
#endif
	u32 UoffsetA, IoffsetA, val, i;
	double samples[2500 * 4];

	ATM90_Init();
//	ATM90_SoftwareReset();
//	ATM90_CalCurrentPhaseAOffset();

#if 0
	while (1) {
			Ua = ATM90_GetRMSVoltage(UaMeasurement);
			Ub = ATM90_GetRMSVoltage(UbMeasurement);
			Uc = ATM90_GetRMSVoltage(UcMeasurement);

			Ia = ATM90_GetRMSCurrent(IaMeasurement);
			Ib = ATM90_GetRMSCurrent(IbMeasurement);
			Ic = ATM90_GetRMSCurrent(IcMeasurement);

		printf("Ua %3.4f Ub %3.4f Uc %3.4f ", Ua, Ub, Uc);
		printf("Ia %3.4f Ib %3.4f Ic %3.4f  \n", Ia, Ib, Ic);
		usleep(DELAY_200_MS);
	}
#endif

#if 0
	ATM90_EnableSPIWrites();
	while(1)
	{
		ATM90_WriteRegister(P3_IOffsetA, 1);
		ATM90_WriteRegister(P3_UOffsetA, 1);
		UoffsetA = ATM90_ReadRegister(P3_IOffsetA);
		IoffsetA = ATM90_ReadRegister(P3_IOffsetA);
		printf("UoffsetA 0x%x IoffsetA 0x%x\n", UoffsetA, IoffsetA);
		ATM90_WriteRegister(P3_IOffsetA, 0xafaf);
		ATM90_WriteRegister(P3_UOffsetA, 0xfafa);
		UoffsetA = ATM90_ReadRegister(P3_IOffsetA);
		IoffsetA = ATM90_ReadRegister(P3_IOffsetA);
		printf("UoffsetA 0x%x IoffsetA 0x%x\n", UoffsetA, IoffsetA);
		usleep(DELAY_200_MS);
	}
#endif

#if 0
	for (i = 0 ; i < 2500 * 4 ; i++)
	{
		samples[i] = ATM90_GetRMSVoltage(UaMeasurement);
	}
	for (i = 0 ; i < 2500 * 4 ; i++)
	{
		printf("%3.4f\n", samples[i]);

	}
#endif
	ATM90_EnableSPIWrites();
	ATM90_WriteRegister(P3_ZXConfig, 0);
	while(1)
	{
		Ua = ATM90_GetRMSVoltage(UaMeasurement);
		Ub = ATM90_GetRMSVoltage(UbMeasurement);
		Uc = ATM90_GetRMSVoltage(UcMeasurement);

		Ia = ATM90_GetRMSCurrent(IaMeasurement);
		Ib = ATM90_GetRMSCurrent(IbMeasurement);
		Ic = ATM90_GetRMSCurrent(IcMeasurement);

	printf("Ua %3.4f Ub %3.4f Uc %3.4f ", Ua, Ub, Uc);
	printf("Ia %3.4f Ib %3.4f Ic %3.4f  \n", Ia, Ib, Ic);
	usleep(DELAY_200_MS);
	}
	return 0;
}

static void ATM90_CalCurrentPhaseAOffset(void)
{
	u32 Ia;
	u16 write_val;

	// Read & print Ia
	Ia = ATM90_GetRMSCurrent(IaMeasurement);
	printf("Initial Ia = 0x%x\n", Ia);

	// Right shift value 7 bits
	Ia = Ia >> 7;

	// Invert and add 1 (2s complement)
	Ia = ~Ia + 1;
	write_val = (u16) (Ia & 0xFFFF);

	// Write lower 16 bits into offset register
	ATM90_EnableSPIWrites();
	ATM90_WriteRegister(P3_IOffsetA, write_val);
	ATM90_DisableSPIWrites();

	// Read & print Ia
	Ia = ATM90_GetRMSCurrent(IaMeasurement);
	printf("write_val 0x%x Cal'd Ia = 0x%x\n", write_val, Ia);
}

static void ATM90_SoftwareReset(void)
{
	ATM90_WriteRegister(P3_2AS_SoftReset, MAGIC_SOFTWARE_RESET_NUMBER);
}

// Init the SPI
static void ATM90_Init(void)
{
	spi_mode_t spi_mode_val = SPI_MODE3;
	spi = spi_init_raw(EDISON_SPI_SSP5, ATM90_CS_PIN_1);
	spi_frequency(spi, SPI_FREQ_1MHZ);
	spi_mode(spi, spi_mode_val);
}

static void ATM90_EnableSPIWrites(void)
{
	ATM90_WriteRegister(P3_2AS_CfgRegAccEn, MAGIC_REGISTER_ACCESS_NUMBER);
}

static void ATM90_DisableSPIWrites(void)
{
	ATM90_WriteRegister(P3_2AS_CfgRegAccEn, MAGIC_REGISTER_ACCESS_NUMBER + 1);
}

static u32 ATM90_ReadRegister(u16 register_addr)
{
	uint8_t txbuf[] =
	{
		ATM90_MSB_READ,
		register_addr,
		0xFF,
		0xFF
	};
	uint8_t rxbuf[] =
	{
		0x00,
		0x00,
		0x00,
		0x00
	};

	spi_transfer_buf(spi, txbuf, rxbuf, 4);
	return (rxbuf[3] + (rxbuf[2]<<8));
}

static void ATM90_WriteRegister(u16 register_addr, u16 value)
{
	uint8_t txbuf[] =
	{
		ATM90_MSB_WRITE,
		register_addr,
		(u8) ((value>>8) & 0xFF),
		(u8) (value & 0xFF),
	};
	uint8_t rxbuf[] =
	{
		0x00,
		0x00,
		0x00,
		0x00
	};

	spi_transfer_buf(spi, txbuf, rxbuf, 4);
}

static double ATM90_GetRMSVoltage(V_RMS_TYPE phase)
{
	u32 val;
	double ret_val;

	switch (phase)
	{
		case UaMeasurement:
			val =ATM90_ReadMeasureParameter(P3_UrmsA, P3_UrmsALSB);
			break;
		case UbMeasurement:
			val =ATM90_ReadMeasureParameter(P3_UrmsB, P3_UrmsBLSB);
			break;
		case UcMeasurement:
			val =ATM90_ReadMeasureParameter(P3_UrmsC, P3_UrmsCLSB);
			break;
		default:
			// Should never get here
			ASSERT(0);
			break;
	}

	val =val >> 6;
	ret_val = (double) val;
	ret_val /=  (double) (1<<16);
	return ret_val;
}

static double ATM90_GetRMSCurrent(I_RMS_TYPE phase)
{
	u32 val;
	double ret_val;

	switch (phase)
	{
		case IaMeasurement:
			val =ATM90_ReadMeasureParameter(P3_IrmsA, P3_IrmsALSB);
			break;
		case IbMeasurement:
			val =ATM90_ReadMeasureParameter(P3_IrmsB, P3_IrmsBLSB);
			break;
		case IcMeasurement:
			val =ATM90_ReadMeasureParameter(P3_IrmsC, P3_IrmsCLSB);
			break;
		default:
			// Should never get here
			ASSERT(0);
			break;
	}

	val =val >> 6;
	ret_val = (double) val;
	//ret_val /=  (double) (1<<16);
	return ret_val;
}


static u32 ATM90_ReadMeasureParameter(u16 regh_addr, u16 regl_addr)
{
	u32	val, val_h, val_l;

	val_h = ATM90_ReadRegister(regh_addr);
	val_l = ATM90_ReadRegister(regl_addr);
	val  = val_h << 16;
	val |= val_l;

	return(val);
}

