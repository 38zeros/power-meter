/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

//
//	Includes
//
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdbool.h>
#include "cs2300.h"
#include "spi.h"
#include "gpio.h"

//
//	Defines
//
#define EDISON_SPI_SSP5						(5)			// spidev5.1
#define CS5484_CS_PIN_1						(1)			// "
#define SPI_FREQ_420KHZ					(420000)
#define LINE_FREQUENCY						(50)		// Note: Same value is used for both 50 and 60 Hz

//
//	Static Data
//
static spi_context spi;

//
//	Static Functions
//
static void CS2300_Init(spi_mode_t spi_mode_val);
static void CS2300_WriteSingle(u8 value);

void CS2300_Start(void)
{
	CS2300_Init(SPI_MODE3);
	CS2300_config();
	/*
	while () {
		device_control_t control = CS2300_send_data(&control.b, 1, MAP_DEV_CTRL);
	}
	*/
	usleep(4*1000*1000);	// Maximum PLL Lock Delay from datasheet
}

	// Init the SPI
static void CS2300_Init(spi_mode_t spi_mode_val)
{
	spi = spi_init_raw(EDISON_SPI_SSP5, CS5484_CS_PIN_1);
	spi_frequency(spi, SPI_FREQ_420KHZ);
	spi_mode(spi, spi_mode_val);
}


void CS2300_send_data(u8* data, u8 length, u8 start_reg) {
	u8 i;
	
	gpio_set(CS2300_CHIP_SELECT, 0);
	
	CS2300_WriteSingle(CS2300_ADDRESS);
	
	if (length > 1)
	{
		//If writing to multiple registers, need incr bit
		CS2300_WriteSingle(start_reg | (1 << 7));
	} 
	else
	{
		CS2300_WriteSingle(start_reg);
	}
	
	for (i=0; i<length; i++) {
		CS2300_WriteSingle(data[i]);
	}

	gpio_set(CS2300_CHIP_SELECT, 1);
}

// Example calc:
//		Desired output freq = 16.384 MHz
// 		Input freq = 60 Hz
//
//		(16.384 MHz / 60 Hz) * 4096 = 0x42AAA000
//
void CS2300_set_ratio(u8 hz)
{
	u8 temp[4];

	if (hz == 60)
	{
		temp[0] = 0x42;
		temp[1] = 0xAA;
		temp[2] = 0xA0;
		temp[3] = 0x00;
	}
	else if (hz == 50)
	{
		temp[0] = 0x50;
		temp[1] = 0x00;
		temp[2] = 0x00;
		temp[3] = 0x00;
	}
	else
	{
		printf ("Invalid Hz value in CS2300_set_ratio()\n");
	}
	CS2300_send_data(temp, 4, MAP_RATIO_MSB);
}

void CS2300_config(void)
{
	// Lock PLL, disable aux and output
	device_control_t control =
	{
		{
				.unlock = 0,
				.aux_output_disable = 1,
				.clock_out_disable = 1
		}
	};
	
	CS2300_send_data(&control.b, 1, MAP_DEV_CTRL);
	
	// Set R_mod to x1, aux is pll lock signal
	device_config_t device_config =
	{
		{
				.r_mod_sel = R_MOD_X1,
				.aux_src_sel = CLK_IN,
				.enable_config = 1
		}
	};
	
	CS2300_send_data(&device_config.b, 1, MAP_DEV_CFG);
	
	// Update configs
	global_config_t global_config =
	{
		{
				.freeze = 0,
				.enable_config = 1
		}
	};
	
	CS2300_send_data(&global_config.b, 1, MAP_GLOBAL_CFG);
	
	// Set ratio
	CS2300_set_ratio(LINE_FREQUENCY);
	
	// Clock skip is enabled, unlock = high
	function_config1_t cfg1 =
	{
		{
				.clock_skip_enable = 1,
				.aux_lock_config = 0,
				.enable_config = 1
		}
	};
	
	CS2300_send_data(&cfg1.b, 1, MAP_FUNC_CFG1);
	
	// Clock is still running when PLL is unlocked, LF ratio is high accuracy
	function_config2_t cfg2 =
	{
		{
				.clock_out_unlock = 1,
				.lf_ratio_config = HIGH_MULTIPLIER
		}
	};
	
	CS2300_send_data(&cfg2.b, 1, MAP_FUNC_CFG2);
	
	// Clock input bandwidth commentary from Hal:
	/*
	 * I seems there are problems with both � a low bandwidth makes capture difficult and a high bandwidth is very susceptible to single cycle events.
	If there is a happy spot , it is probably a few hertz so that a single bad error signal (by a small amount) does not a cause a �full-slip� in the clock.

	I think a single cycle error signal is the cause of the upsets we are seeing.
	In addition to the other sources I mentioned, I now suspect a slight shift in ground reference is likely the source.
	I think an asymmetrical load on the phases (either internal of external) will shift the ground reference.
	I think changes in the reactive load will also cause a small shift in the relative voltage phases.
	These effect become larger as the power grid resistance goes up.
	This does not matter long term, but the cycle when the shift occurs will measure either short or long.

	In our Beaverton office, I  watched the voltage between neutral (white wire) and the ground connection at the outlet.
	This had a lot of step changes on the order of volts.
	 */
	function_config3_t cfg3 =
	{
		{.clkin_bw = MIN_BW_8HZ}
	};
	
	CS2300_send_data(&cfg3.b, 1, MAP_FUNC_CFG3);
	
	// Enable output clock
	control.unlock = 0;
	control.aux_output_disable = 0;
	control.clock_out_disable = 0;
	
	CS2300_send_data(&control.b, 1, MAP_DEV_CTRL);
	
	return;
}

// rxbuf should always be zeros
static void CS2300_WriteSingle(u8 value)
{
	u8 txbuf[] =
	{
			value
	};
	u8 rxbuf[] =
	{
			0x00,
	};

	spi_transfer_buf(spi, txbuf, rxbuf, 1);
}
