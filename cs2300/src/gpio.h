/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#ifndef GPIO_H_
#define GPIO_H_
#include "types.h"
#include <stdbool.h>

typedef enum {
	GPIO130 = 130,
	CS2300_CHIP_SELECT = GPIO130,
} GpioPin;

typedef enum {
	DIRECTION_INPUT,
	DIRECTION_OUTPUT
} GpioDirection;

typedef struct {
	GpioPin pin;
	GpioDirection direction;
	u8 initial_value;
} GpioDesc;

void gpio_init_gpio(void);
void gpio_export(u32 gpio);
void gpio_set_direction(u32 gpio, GpioDirection direction);
void gpio_set(u32 gpio, bool value);
bool gpio_get(u32 gpio);

#endif /* GPIO_H_ */
