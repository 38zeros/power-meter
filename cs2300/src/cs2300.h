/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#ifndef CS2300_H_
#define CS2300_H_
#include "types.h"

#define CS2300_ADDRESS	0x9E	//0b10011110

// Memory address pointers
#define MAP_CLOCK_ID		0x01
#define MAP_DEV_CTRL	0x02
#define MAP_DEV_CFG		0x03
#define MAP_GLOBAL_CFG	0x05
#define MAP_RATIO_MSB	0x06
#define MAP_RATIO_2SB	0x07
#define MAP_RATIO_3SB	0x08
#define MAP_RATIO_LSB	0x09
#define MAP_FUNC_CFG1	0x16
#define MAP_FUNC_CFG2	0x17
#define MAP_FUNC_CFG3	0x1E

// Values for device_config.r_mod_sel
#define R_MOD_X1	0x0
#define R_MOD_X2	0x1
#define R_MOD_X4	0x2
#define R_MOD_X8	0x3
#define R_MOD_DIV2	0x4
#define R_MOD_DIV4	0x5
#define R_MOD_DIV8	0x6
#define R_MOD_DIV16	0x7

// Values for device_config.aux_src_sel
#define CLK_IN		0x1
#define CLK_OUT		0x2
#define PLL_LOCK	0x3

// Values for function_config1.aux_lock_config
#define ACTIVE_HIGH	0x0
#define ACTIVE_LOW	0x1

// Values for function_config2.lf_ratio_config
#define HIGH_MULTIPLIER	0x0
#define HIGH_ACCURACY	0x1

// Values for function_config3.clkin_bw
#define MIN_BW_1HZ		0x0
#define MIN_BW_2HZ		0x1
#define MIN_BW_4HZ		0x2
#define MIN_BW_8HZ		0x3
#define MIN_BW_16HZ		0x4
#define MIN_BW_32HZ		0x5
#define MIN_BW_64HZ		0x6
#define MIN_BW_128HZ	0x7

typedef union{
	struct {
		u8 revision:3;
		u8 device:5;
	};
	u8 b;
}device_id_t;

typedef union{
	struct {
		u8 clock_out_disable:1;
		u8 aux_output_disable:1;
		u8 :5;
		u8 unlock:1;
	};
	u8 b;
}device_control_t;

typedef union{
	struct {
		u8 enable_config:1;
		u8 aux_src_sel:2;
		u8 :2;
		u8 r_mod_sel:3;
	};
	u8 b;
}device_config_t;

typedef union{
	struct {
		u8 enable_config:1;
		u8 :2;
		u8 freeze:1;
		u8 :4;
	};
	u8 b;
}global_config_t;

typedef union {
	struct {
		u8 ratio_msb;
		u8 ratio_2sb;
		u8 ratio_3sb;
		u8 ratio_lsb;
	};
	u32 b;
	u8 c[4];
}ratio_t;

typedef union{
	struct {
		u8 :4;
		u8 enable_config:1;
		u8 :1;
		u8 aux_lock_config:1;
		u8 clock_skip_enable:1;
	};	
	u8 b;
}function_config1_t;

typedef union{
	struct {
		u8 :3;
		u8 lf_ratio_config:1;
		u8 clock_out_unlock:1;
		u8 :3;
	};
	u8 b;
}function_config2_t;

typedef union{
	struct {
		u8 :4;
		u8 clkin_bw:3;
		u8 :1;
	};
	u8 b;
}function_config3_t;

// Function prototypes
void CS2300_send_data(u8* data, u8 length, u8 start_reg);
void CS2300_set_ratio(u8 hz);
void CS2300_config(void);
void CS2300_Start(void);
#endif /* CS2300_H_ */
