/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include "spi.h"
#include "types.h"

#define MAX_SIZE 64
#define SPI_MAX_LENGTH 4096

#define DEBUG_PRINTF(...)    printf(__VA_ARGS__)

int* plat = NULL;
/**
 * A structure representing the SPI device
 */
struct _spi {
    int devfd; /**< File descriptor to SPI Device */
    int mode; /**< Spi mode see spidev.h */
    int clock; /**< clock to run transactions at */
    bool lsb; /**< least significant bit mode */
    unsigned int bpw; /**< Bits per word */
};

spi_context spi_init_raw(unsigned int bus, unsigned int cs)
{

	DEBUG_PRINTF("spi_init_raw() called.\n");
    spi_context dev = (spi_context) malloc(sizeof(struct _spi));
    if (dev == NULL) {
        DEBUG_PRINTF("spi: Failed to allocate memory for context");
        return NULL;
    }
    memset(dev, 0, sizeof(struct _spi));

    char path[MAX_SIZE];
    sprintf(path, "/dev/spidev%u.%u", bus, cs);

    dev->devfd = open(path, O_RDWR);
    if (dev->devfd < 0) {
    	DEBUG_PRINTF("spi: Failed opening SPI Device. bus:%s", path);
        free(dev);
        return NULL;
    }
    dev->bpw = 8;
    dev->clock = 4000000;
    dev->lsb = 0;
    dev->mode = 0;

    return dev;
}

result_t spi_mode(spi_context dev, spi_mode_t mode)
{
    uint8_t spi_mode = 0;
    switch (mode) {
        case SPI_MODE0:
            spi_mode = SPI_MODE_0;
            break;
        case SPI_MODE1:
            spi_mode = SPI_MODE_1;
            break;
        case SPI_MODE2:
            spi_mode = SPI_MODE_2;
            break;
        case SPI_MODE3:
            spi_mode = SPI_MODE_3;
            break;
        default:
            spi_mode = SPI_MODE_0;
            break;
    }

    if (ioctl (dev->devfd, SPI_IOC_WR_MODE, &spi_mode) < 0) {
    	DEBUG_PRINTF("spi: Failed to set spi mode");
        return ERROR_INVALID_RESOURCE;
    }

    dev->mode = spi_mode;
    return SUCCESS;
}

result_t spi_frequency(spi_context dev, int hz)
{
    dev->clock = hz;
    return SUCCESS;
}

result_t spi_lsbmode(spi_context dev, bool lsb)
{
    uint8_t lsb_mode = (uint8_t) lsb;
    if (ioctl (dev->devfd, SPI_IOC_WR_LSB_FIRST, &lsb_mode) < 0) {
    	DEBUG_PRINTF("spi: Failed to set bit order");
        return ERROR_INVALID_RESOURCE;
    }
    if (ioctl (dev->devfd, SPI_IOC_RD_LSB_FIRST, &lsb_mode) < 0) {
    	DEBUG_PRINTF("spi: Failed to set bit order");
        return ERROR_INVALID_RESOURCE;
    }
    dev->lsb = lsb;
    return SUCCESS;
}

result_t spi_bit_per_word(spi_context dev, unsigned int bits)
{
    if (ioctl(dev->devfd, SPI_IOC_WR_BITS_PER_WORD, &bits) < 0) {
    	DEBUG_PRINTF("spi: Failed to set bit per word");
        return ERROR_INVALID_RESOURCE;
    }
    dev->bpw = bits;
    return SUCCESS;
}

uint8_t spi_write(spi_context dev, uint8_t data)
{
    struct spi_ioc_transfer msg;
    memset(&msg, 0, sizeof(msg));

    uint16_t length = 1;

    uint8_t recv = 0;
    msg.tx_buf = (unsigned long) &data;
    msg.rx_buf = (unsigned long) &recv;
    msg.speed_hz = dev->clock;
    msg.bits_per_word = dev->bpw;
    msg.delay_usecs = 0;
    msg.len = length;
    if (ioctl(dev->devfd, SPI_IOC_MESSAGE(1), &msg) < 0) {
    	DEBUG_PRINTF("spi: Failed to perform dev transfer");
        return -1;
    }
    return recv;
}

result_t spi_transfer_buf(spi_context dev, uint8_t* data, uint8_t* rxbuf, int length)
{
    struct spi_ioc_transfer msg;
    memset(&msg, 0, sizeof(msg));

    msg.tx_buf = (unsigned long) data;
    msg.rx_buf = (unsigned long) rxbuf;
    msg.speed_hz = dev->clock;
    msg.bits_per_word = dev->bpw;
    msg.delay_usecs = 0;
    msg.len = length;
    if (ioctl(dev->devfd, SPI_IOC_MESSAGE(1), &msg) < 0) {
    	DEBUG_PRINTF( "spi: Failed to perform dev transfer");
        return ERROR_INVALID_RESOURCE;
    }
    return SUCCESS;
}

uint8_t* spi_write_buf(spi_context dev, uint8_t* data, int length)
{
    uint8_t* recv = malloc(sizeof(uint8_t) * length);

    if (spi_transfer_buf(dev, data, recv, length) != SUCCESS) {
    	free(recv);
        return NULL;
    }
    return recv;
}

result_t spi_stop(spi_context dev)
{
    close(dev->devfd);
    free(dev);
    return SUCCESS;
}
