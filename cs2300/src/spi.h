/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#ifndef __SPI_H
#define __SPI_H

#include <stdio.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdbool.h>
#include "types.h"

/**
 * SPI Modes
 */
typedef enum {
    SPI_MODE0 = 0, /**< CPOL = 0, CPHA = 0, Clock idle low, data is clocked in on rising edge, output data (change) on falling edge */
    SPI_MODE1 = 1, /**< CPOL = 0, CPHA = 1, Clock idle low, data is clocked in on falling edge, output data (change) on rising edge */
    SPI_MODE2 = 2, /**< CPOL = 1, CPHA = 0, Clock idle low, data is clocked in on falling edge, output data (change) on rising edge */
    SPI_MODE3 = 3, /**< CPOL = 1, CPHA = 1, Clock idle low, data is clocked in on rising, edge output data (change) on falling edge */
} spi_mode_t;

/**
 * Opaque pointer definition to the internal struct _spi
 */
typedef struct _spi* spi_context;

/**
 * Initialise SPI_context, uses board mapping. Sets the muxes
 *
 * @param bus Bus to use, as listed in platform definition, normally 0
 * @return Spi context or NULL
 */
void spi_init(int bus);

/**
 * Initialise SPI_context without any board configuration, selects a bus and a mux.
 *
 * @param bus Bus to use as listed by spidev
 * @param cs Chip select to use as listed in spidev
 * @return Spi context or NULL
 */
spi_context spi_init_raw(unsigned int bus, unsigned int cs);

/**
 * Set the SPI device mode. see spidev 0-3.
 *
 * @param dev The Spi context
 * @param mode The SPI mode, See Linux spidev
 * @return Spi context or NULL
 */
result_t spi_mode(spi_context dev, spi_mode_t mode);

/** Set the SPI device operating clock frequency.
 *
 * @param dev the Spi context
 * @param hz the frequency in hz
 * @return spi_context The returned initialised SPI context
 */
result_t spi_frequency(spi_context dev, int hz);

/** Write Single Byte to the SPI device.
 *
 * @param dev The Spi context
 * @param data Data to send
 * @return Data received on the miso line
 */
uint8_t spi_write(spi_context dev, uint8_t data);

/** Write Buffer of bytes to the SPI device. The pointer return has to be
 * free'd by the caller. It will return a NULL pointer in cases of error.
 *
 * @param dev The Spi context
 * @param data to send
 * @param length elements within buffer, Max 4096
 * @return Data received on the miso line, same length as passed in
 */
uint8_t* spi_write_buf(spi_context dev, uint8_t* data, int length);

/** Transfer Buffer of bytes to the SPI device. Both send and recv buffers
 * are passed in
 *
 * @param dev The Spi context
 * @param data to send
 * @param rxbuf buffer to recv data back, may be NULL
 * @param length elements within buffer, Max 4096
 * @return Result of operation
 */
result_t spi_transfer_buf(spi_context dev, uint8_t* data, uint8_t* rxbuf, int length);

/**
 * Change the SPI lsb mode
 *
 * @param dev The Spi context
 * @param lsb Use least significant bit transmission. 0 for msbi
 * @return Result of operation
 */
result_t spi_lsbmode(spi_context dev, bool lsb);

/**
 * Set bits per mode on transaction, defaults at 8
 *
 * @param dev The Spi context
 * @param bits bits per word
 * @return Result of operation
 */
result_t spi_bit_per_word(spi_context dev, unsigned int bits);

/**
 * De-inits an spi_context device
 *
 * @param dev The Spi context
 * @return Result of operation
 */
result_t spi_stop(spi_context dev);

#endif
