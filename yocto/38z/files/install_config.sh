#!/bin/sh

systemctl disable edison_config.service
mkdir /home/root/.ssh
cp -av /etc/38z/.profile /home/root/
cp -av /etc/38z/journald.conf /etc/systemd/journald.conf
sync

# Basic connectivity 
cp -av /etc/38z/turn-on-telit.sh /home/root/
cp -av /etc/38z/38z-telit.service /lib/systemd/system/38z-telit.service
cp -av /etc/38z/38z-ppp.service /lib/systemd/system/38z-ppp.service
chmod 644 /lib/systemd/system/38z-telit.service
chmod 644 /lib/systemd/system/38z-ppp.service
systemctl enable 38z-telit.service
systemctl enable 38z-ppp.service
sync

# Enable display
cp -av /etc/38z/ssd1309.sh /home/root/
cp -av /etc/38z/ssd1309 /home/root/
cp -av /etc/38z/ssd1309.service /lib/systemd/system/ssd1309.service
chmod 644 /lib/systemd/system/ssd1309.service
systemctl enable ssd1309.service
sync

# Configure/enable PMIC and support chips
cp -av /etc/38z/cs2300 /home/root/
cp -av /etc/38z/take-cs5484-out-of-reset.sh /home/root/
cp -av /etc/38z/cs5484.service /lib/systemd/system/cs5484.service
chmod 644 /lib/systemd/system/cs5484.service

# Connection-maintenance brutality 
cp -av /etc/38z/autossh.service /lib/systemd/system/autossh.service
chmod 644 /lib/systemd/system/autossh.service
systemctl enable autossh.service
sync

# This can make it hard to debug a device locally if connectivity doesn't work
#cp -av /etc/38z/watchdog.service /lib/systemd/system/watchdog.service
#cp -av /etc/38z/watchdog.timer /lib/systemd/system/watchdog.timer
#chmod 644 /lib/systemd/system/watchdog.service
#chmod 644 /lib/systemd/system/watchdog.timer
#systemctl enable watchdog.service
#systemctl enable watchdog.timer
#sync

systemctl daemon-reload

source /home/root/.profile
cp -av /etc/ppp/aeris/gprs-connect /etc/ppp/
cp -av /etc/ppp/aeris/gprs-disconnect /etc/ppp/
cp -av /etc/ppp/aeris/gprs-options /etc/ppp/
cp -av /etc/ppp/aeris/pap-secrets /etc/ppp/
cp -av /etc/ppp/aeris/ppp-start /etc/ppp/
sync
