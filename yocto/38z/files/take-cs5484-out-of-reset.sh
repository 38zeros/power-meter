#/bin/ash

# Start the PLL which serves as a clock to both cs5484s
/home/root/cs2300

# Actually take both cs5484s out of reset
/bin/echo 41 > /sys/class/gpio/export
/bin/echo mode0 > /sys/kernel/debug/gpio_debug/gpio41/current_pinmux
/bin/echo 'out' > /sys/class/gpio/gpio41/direction
/bin/echo 1 > /sys/class/gpio/gpio41/value

