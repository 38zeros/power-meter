#/bin/ash

# Set Telit's ON_OFF signal low to start
echo 28 > /sys/class/gpio/export
echo mode0 > /sys/kernel/debug/gpio_debug/gpio28/current_pinmux
echo 'out' > /sys/class/gpio/gpio28/direction
echo 0 > /sys/class/gpio/gpio28/value

# Turn off Telit's shutdown
echo 27 > /sys/class/gpio/export
echo mode0 > /sys/kernel/debug/gpio_debug/gpio27/current_pinmux
echo 'out' > /sys/class/gpio/gpio27/direction
echo 0 > /sys/class/gpio/gpio27/value

# Take USB hub out of reset
sleep 5
echo mode0 > /sys/kernel/debug/gpio_debug/gpio131/current_pinmux
echo 'out' > /sys/class/gpio/gpio131/direction
echo 1 > /sys/class/gpio/gpio131/value
                                                             
# Telit toggle ON_OFF                                                 
sleep 5                                                        
echo 28 > /sys/class/gpio/export                                
echo mode0 > /sys/kernel/debug/gpio_debug/gpio28/current_pinmux
echo 'out' > /sys/class/gpio/gpio28/direction
echo 1 > /sys/class/gpio/gpio28/value
sleep 7                              
echo 0 > /sys/class/gpio/gpio28/value
