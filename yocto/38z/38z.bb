DESCRIPTION = "Custom scripts, apps and config for 38 Zeros"
SECTION = "support"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS_prepend := "${THISDIR}/files/"

SRC_URI = "	file://gprs-connect 			\
	   		file://resolv.conf	 			\
	   		file://profile		 			\
	   		file://install_config.sh    	\
	   		file://journald.conf    		\
	   		file://gprs-disconnect	 		\
	   		file://gprs-options 			\
	   		file://pap-secrets 				\
   			file://turn-on-telit.sh  		\
	   		file://turn-on-telit-2.sh  		\
	   		file://38z-ppp.service  		\
	   		file://38z-telit.service  		\
	   		file://cs5484.service  			\
	   		file://take-cs5484-out-of-reset.sh  		\
	   		file://ssd1309.sh  		\
	   		file://cs2300  								\
	   		file://ssd1309  							\
	   		file://ssd1309.service 						\
	   		file://ppp-start"	 		

S = "${WORKDIR}"

#INSANE_SKIP_${PN} = "installed-vs-shipped"

do_install() {
        install -v -d  ${D}/etc/38z/
        install -v -d  ${D}/etc/ppp/aeris/
        install -v -d  ${D}/etc/config
        install -v -d  ${D}/etc/config/38z/
        install -v -d  ${D}/etc/systemd/
        
        install -m 0755 ${WORKDIR}/profile ${D}/etc/38z/.profile
        install -m 0755 ${WORKDIR}/turn-on-telit.sh ${D}/etc/38z/turn-on-telit.sh
        install -m 0755 ${WORKDIR}/turn-on-telit-2.sh ${D}/etc/38z/turn-on-telit-2.sh
        install -m 0755 ${WORKDIR}/resolv.conf ${D}/etc/resolv.conf
        install -m 0755 ${WORKDIR}/install_config.sh ${D}/etc/38z/install_config.sh
        install -m 0755 ${WORKDIR}/journald.conf ${D}/etc/38z/journald.conf
        install -m 0755 ${WORKDIR}/gprs-connect ${D}/etc/ppp/aeris/gprs-connect
        install -m 0755 ${WORKDIR}/gprs-disconnect ${D}/etc/ppp/aeris/gprs-disconnect
        install -m 0755 ${WORKDIR}/gprs-options ${D}/etc/ppp/aeris/gprs-options
        install -m 0755 ${WORKDIR}/pap-secrets ${D}/etc/ppp/aeris/pap-secrets
        install -m 0755 ${WORKDIR}/ppp-start ${D}/etc/ppp/aeris/ppp-start
        install -m 0755 ${WORKDIR}/38z-ppp.service ${D}/etc/38z/38z-ppp.service
        install -m 0755 ${WORKDIR}/cs5484.service ${D}/etc/38z/cs5484.service
        install -m 0755 ${WORKDIR}/ssd1309.service ${D}/etc/38z/ssd1309.service
        install -m 0755 ${WORKDIR}/cs2300 ${D}/etc/38z/cs2300
        install -m 0755 ${WORKDIR}/ssd1309 ${D}/etc/38z/ssd1309
        install -m 0755 ${WORKDIR}/take-cs5484-out-of-reset.sh ${D}/etc/38z/take-cs5484-out-of-reset.sh        
        install -m 0755 ${WORKDIR}/ssd1309.sh ${D}/etc/38z/ssd1309.sh  
        install -m 0755 ${WORKDIR}/38z-telit.service ${D}/etc/38z/38z-telit.service
}

FILES_${PN}-dbg += "/etc/38z/.debug"


