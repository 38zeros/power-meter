#/bin/ash

# Toggle GPIO20 indefinitely

while [ 1 ]
do
        echo 1 > /sys/class/gpio/gpio20/value
        echo 0 > /sys/class/gpio/gpio20/value
done
