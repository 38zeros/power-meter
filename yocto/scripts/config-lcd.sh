#/bin/ash

# Take LCD out of reset and set A0 so the I2C address is 0x3d

# Raise NRESET on LCD
echo mode0 > /sys/kernel/debug/gpio_debug/gpio129/current_pinmux
echo 'out' > /sys/class/gpio/gpio129/direction
echo 1 > /sys/class/gpio/gpio129/value

# Set A0
echo mode0 > /sys/kernel/debug/gpio_debug/gpio128/current_pinmux
echo 'out' > /sys/class/gpio/gpio128/direction
echo 1 > /sys/class/gpio/gpio128/value