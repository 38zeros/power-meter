#/bin/ash

# Turn GPIO27 into a normal GPIO

echo 27 > /sys/class/gpio/export
echo mode0 > /sys/kernel/debug/gpio_debug/gpio27/current_pinmux
echo 'out' > /sys/class/gpio/gpio27/direction