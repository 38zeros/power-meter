#/bin/ash

# After the I2C1 - I2C6 swap

echo 20 > /sys/class/gpio/export
echo mode0 > /sys/kernel/debug/gpio_debug/gpio20/current_pinmux
echo 'out' > /sys/class/gpio/gpio20/direction
echo 1 > /sys/class/gpio/gpio20/value
sleep 5
echo 0 > /sys/class/gpio/gpio20/value
