#/bin/ash

# Turn on CS2300 PLL to drive both power chips at 4.096MHz

# Raise reset on the CS2300
echo 41 > /sys/class/gpio/export
echo mode0 > /sys/kernel/debug/gpio_debug/gpio41/current_pinmux
echo 'out' > /sys/class/gpio/gpio41/direction
echo 1 > /sys/class/gpio/gpio41/value

# Execute the cs2300 app (this may be rolled into the main app in the future)
~/cs2300

