#/bin/ash

# Turn GPIO28 into a normal GPIO

echo 28 > /sys/class/gpio/export
echo mode0 > /sys/kernel/debug/gpio_debug/gpio28/current_pinmux
echo 'out' > /sys/class/gpio/gpio28/direction
