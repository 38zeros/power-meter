#/bin/ash

# Turn GPIO20 into a normal GPIO

echo 20 > /sys/class/gpio/export
echo mode0 > /sys/kernel/debug/gpio_debug/gpio20/current_pinmux
echo 'out' > /sys/class/gpio/gpio20/direction