#/bin/ash

# Before the I2C1 - I2C6 swap

echo 28 > /sys/class/gpio/export
echo mode0 > /sys/kernel/debug/gpio_debug/gpio28/current_pinmux
echo 'out' > /sys/class/gpio/gpio28/direction  
echo 1 > /sys/class/gpio/gpio28/value
sleep 5
echo 0 > /sys/class/gpio/gpio28/value

