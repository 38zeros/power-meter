/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#ifndef __TYPES_H
#define __TYPES_H

typedef enum {
    SUCCESS                              =  0, /**< Expected response */
    ERROR_FEATURE_NOT_IMPLEMENTED        =  1, /**< Feature TODO */
    ERROR_FEATURE_NOT_SUPPORTED          =  2, /**< Feature not supported by HW */
    ERROR_INVALID_VERBOSITY_LEVEL        =  3, /**< Verbosity level wrong */
    ERROR_INVALID_PARAMETER              =  4, /**< Parameter invalid */
    ERROR_INVALID_HANDLE                 =  5, /**< Handle invalid */
    ERROR_NO_RESOURCES                   =  6, /**< No resource of that type avail */
    ERROR_INVALID_RESOURCE               =  7, /**< Resource invalid */
    ERROR_INVALID_QUEUE_TYPE             =  8, /**< Queue type incorrect */
    ERROR_NO_DATA_AVAILABLE              =  9, /**< No data available */
    ERROR_INVALID_PLATFORM               = 10, /**< Platform not recognised */
    ERROR_PLATFORM_NOT_INITIALISED       = 11, /**< Board information not initialised */
    ERROR_PLATFORM_ALREADY_INITIALISED   = 12, /**< Board is already initialised */

    ERROR_UNSPECIFIED                    = 99 /**< Unknown Error */
} result_t;

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

typedef unsigned char           u8;
typedef unsigned short          u16;
typedef unsigned int            u32;
typedef unsigned long long      u64;
typedef signed char             s8;
typedef short                   s16;
typedef int                     s32;
typedef long long               s64;

#endif
