from struct import *
readMask=0x00
writeMask=0x40
pageMask=0x80
instructionMask=0xC0

def SetPage(dev,page):
  txbuf=bytearray(1)
  txbuf[0]=pageMask+page
  dev.write(txbuf)

def ReadReg(dev,page,reg):
  SetPage(dev,page)
  txbuf=bytearray(1)
  txbuf[0]=readMask+reg
  txbuf.append(0x00)
  txbuf.append(0x00)
  txbuf.append(0x00)
  txbuf.append(0x00)
  rxbuf=dev.write(txbuf)
  return rxbuf[1:4]

def WriteReg(dev,page,reg,val):
  SetPage(dev,page)
  txbuf=bytearray(1)
  txbuf[0]=writeMask+reg
  txbuf.append((val>>16)%256)
  txbuf.append((val>>8)%256)
  txbuf.append(val%256)
  for b in txbuf:
    print "%2x"%b,
  print
  rxbuf=dev.write(txbuf)
  return rxbuf

def CalibrateDcOffset(dev):
  txbuf=bytearray(1)
  txbuf[0]=instructionMask+0x26
  dev.write(txbuf)

def Reset(dev):
  txbuf=bytearray(1)
  txbuf[0]=instructionMask+1
  dev.write(txbuf)

def SampleContinuous(dev):
  txbuf=bytearray(1)
  txbuf[0]=instructionMask+0x15
  dev.write(txbuf)

def SampleSingle(dev):
  txbuf=bytearray(1)
  txbuf[0]=instructionMask+0x14
  dev.write(txbuf)

def DecodeVoltage(v):
  v1=""
  for b in v:
    v1+=chr(b)
  v1+=chr(0)
  vout=unpack('>l',v1)
  return (float(vout[0])/(2**31))
