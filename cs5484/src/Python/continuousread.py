import mraa as m
import cs5484
import time
from datetime import datetime
dev = m.Spi(5)
dev.mode(3)
dev.frequency(10000000)

cs5484.Reset(dev)
time.sleep(0.1)
cs5484.SampleContinuous(dev)

cs5484.WriteReg(dev,16,0,0x000002) #enable V1 HPF

while True:
  rxbuf=cs5484.ReadReg(dev,16,3)
  dt=datetime.now()
  #for b in rxbuf:
  #  print("%x"%b)
  print "0x%02x%02x%02x,%d.%d"%(rxbuf[0],rxbuf[1],rxbuf[2],dt.second,dt.microsecond),",",
  print cs5484.DecodeVoltage(rxbuf) 
