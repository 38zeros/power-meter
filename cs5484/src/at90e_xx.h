/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#ifndef AT90E_xx_H_INCLUDED
#define AT90E_xx_H_INCLUDED

typedef	enum
{
	P3_SoftReset		=0,
	P3_MeterEn			=0,		//90E32AS
	P3_SysStatus0		=1,
	P3_ChannelMapI		=1,		//90E32AS
	P3_SysStatus1		=2,
	P3_ChannelMapU		=2,		//90E32AS
	P3_FuncEn0			=3,		//90E32AS invalible
	P3_FuncEn1			=4,		//90E32AS invalible
	P3_SagPeakDetCfg	=5,		//90E32AS
	P3_Ovth				=6,		//90E32AS
	P3_ZXConfig		=7,
	P3_SagTh		=0x08,
	P3_PhaseLossTh	=0x09,
	P3_INWarnTh0	=0x0A,
	P3_INWarnTh1	=0x0B,	//90E32/32A invalible
	P3_Olth			=0x0B,	//90E32AS
	P3_THDNUTh		=0x0C,
	P3_FreqLoTh		=0x0C,	//90E32AS
	P3_THDNITh		=0x0D,
	P3_FreqHiTh		=0x0D,	//90E32AS
	P3_DMACtrl		=0x0E,	//90E32/32A invalible
	P3_PMPwrCtrl	=0x0E,	//90E32AS
	P3_LastSPIData	=0x0F,
	P3_IRQ0MergeCfg	=0x0F,	//90E32AS
	
	P3_DetectCtrl		=0x10,
	P3_DetectThA		=0x11,	
	P3_DetectTh1		=0x11,	//
	P3_DetectThB		=0x12,
	P3_DetectTh2		=0x12,	//
	P3_DetectThC		=0x13,
	P3_DetectTh3		=0x13,	//
	P3_PMOffsetA		=0x14,
	P3_IDCoffsetA		=0x14,	//90E32AS
	P3_PMOffsetB		=0x15,
	P3_IDCoffsetB		=0x15,	//90E32AS
	P3_PMOffsetC		=0x16,
	P3_IDCoffsetC		=0x16,	//90E32AS
	P3_PMPGA			=0x17,
	P3_UDCoffsetA		=0x17,	//90E32AS
	P3_PMIrmsA			=0x18,
	P3_UDCoffsetB		=0x18,	//90E32AS
	P3_PMIrmsB			=0x19,
	P3_UDCoffsetC		=0x19,	//90E32AS
	P3_PMIrmsC			=0x1A,
	P3_UGainTAB			=0x1A,	//90E32AS
	P3_PMconfig			=0x1B,
	P3_UGainTC			=0x1B,	//90E32AS
	P3_PMAvgSamples		=0x1C,
	P3_PhiFreqComp		=0x1C,	//90E32AS
	P3_PMIrmsLSB		=0x1D,	//90E32AS invalible
	
	P3_LOGIrms0			=0x20,	//90E32AS
	P3_LOGIrms1			=0x21,	//90E32AS
	P3_F0				=0x22,	//90E32AS
	P3_T0				=0x23,	//90E32AS
	P3_PhiAIrms01		=0x24,	//90E32AS
	P3_PhiAIrms02		=0x25,	//90E32AS
	P3_GainAIrms01		=0x26,	//90E32AS
	P3_GainAIrms02		=0x27,	//90E32AS	
	P3_PhiBIrms01		=0x28,	//90E32AS
	P3_PhiBIrms02		=0x29,	//90E32AS
	P3_GainBIrms01		=0x2A,	//90E32AS
	P3_GainBIrms02		=0x2B,	//90E32AS	
	P3_PhiCIrms01		=0x2C,	//90E32AS
	P3_PhiCIrms02		=0x2D,	//90E32AS
	P3_GainCIrms01		=0x2E,	//90E32AS
	P3_GainCIrms02		=0x2F,	//90E32AS
	
	P3_ConfigStart		=0x30,	//90E32AS invalible
	P3_PLConstH,
	P3_PLConstL,
	P3_MMode0,
	P3_MMode1,
	P3_PStartTh			=0x35,
	P3_QStartTh,
	P3_SStartTh,
	P3_PPhaseTh,
	P3_QPhaseTh,
	P3_SPhaseTh,
	P3_CS0			=0x3B,	//90E32AS invalible
	P3_CalStart		=0x40,	//90E32AS invalible
	P3_POffsetA,
	P3_QOffsetA,
	P3_POffsetB,
	P3_QOffsetB,
	P3_POffsetC,
	P3_QOffsetC		=0x46,
	P3_PQGainA		=0x47,
	P3_GainA		=0x47,	//90E32AS
	P3_PhiA			=0x48,
	P3_PQGainB		=0x49,
	P3_PhiB,
	P3_PQGainC,
	P3_PhiC,
	P3_CS1			=0x4D,	//90E32AS invalible
	P3_HarmStart	=0x50,	//90E32AS invalible
	P3_POffsetAF,
	P3_POffsetBF,
	P3_POffsetCF,
	P3_PGainAF,
	P3_PGainBF,
	P3_PGainCF,
	P3_CS2			=0x57,	//90E32AS invalible
	P3_AdjStart		=0x60,	//90E32AS invalible
	P3_UGainA,
	P3_IGainA,
	P3_UOffsetA,
	P3_IOffsetA,
	P3_UGainB,
	P3_IGainB,
	P3_UOffsetB,
	P3_IOffsetB,
	P3_UGainC,
	P3_IGainC,
	P3_UOffsetC,
	P3_IOffsetC			=0x6C,
	P3_IGainN			=0x6D,	//90E36
	P3_IOffsetN			=0x6E,	//90E36
	P3_CS3				=0x6F,	//90E32AS invalible
	
	P3_2AS_SoftReset	=0x70,	//90E32AS
	P3_2AS_EMMState0	=0x71,	//90E32AS
	P3_2AS_EMMState1	=0x72,	//90E32AS
	P3_2AS_EMMIntState0	=0x73,	//90E32AS
	P3_2AS_EMMIntState1	=0x74,	//90E32AS
	P3_2AS_EMMIntEn0	=0x75,	//90E32AS
	P3_2AS_EMMIntEn1	=0x76,	//90E32AS
	P3_2AS_LastSPIData	=0x78,	//90E32AS
	P3_2AS_CRCErrStatus	=0x79,	//90E32AS
	P3_2AS_CRCDigest	=0x7A,	//90E32AS
	P3_2AS_CfgRegAccEn	=0x7F,	//90E32AS	
	
	P3_APenergyT		=0x80,
	P3_APenergyA,
	P3_APenergyB,
	P3_APenergyC,
	P3_ANenergyT,
	P3_ANenergyA,
	P3_ANenergyB,
	P3_ANenergyC,
	P3_RPenergyT,
	P3_RPenergyA,
	P3_RPenergyB,
	P3_RPenergyC,
	P3_RNenergyT,
	P3_RNenergyA,
	P3_RNenergyB,
	P3_RNenergyC,
	P3_SAenergyT		=0x90,
	P3_SenergyA,
	P3_SenergyB,
	P3_SenergyC,
	P3_SVenergyT		=0x94,	//90E32/32A/32AS invalible
	P3_EnStatus0,				//90E32AS invalible
	P3_EnStatus1		=0x96,	//90E32AS invalible
	P3_SVmeanT			=0x98,	//90E32/32A/32AS invalible
	P3_SVmeanTLSB		=0x99,	//90E32/32A/32AS invalible
	P3_APenergyTF		=0xA0,
	P3_APenergyAF,
	P3_APenergyBF,
	P3_APenergyCF,
	P3_ANenergyTF,
	P3_ANenergyAF,
	P3_ANenergyBF,
	P3_ANenergyCF,
	P3_APenergyTH,
	P3_APenergyAH,
	P3_APenergyBH,
	P3_APenergyCH,
	P3_ANenergyTH,
	P3_ANenergyAH,
	P3_ANenergyBH,
	P3_ANenergyCH,
	P3_PmeanT           =0xB0,
	P3_PmeanA,
	P3_PmeanB,
	P3_PmeanC,
	P3_QmeanT,
	P3_QmeanA,
	P3_QmeanB,
	P3_QmeanC,
	P3_SAmeanT,
	P3_SmeanA,
	P3_SmeanB,
	P3_SmeanC,
	P3_PFmeanT,
	P3_PFmeanA,
	P3_PFmeanB,
	P3_PFmeanC,
	P3_PmeanTLSB,
	P3_PmeanALSB,
	P3_PmeanBLSB,
	P3_PmeanCLSB,
	P3_QmeanTLSB,
	P3_QmeanALSB,
	P3_QmeanBLSB,
	P3_QmeanCLSB,
	P3_SAmeanTLSB,
	P3_SmeanALSB,
	P3_SmeanBLSB,
	P3_SmeanCLSB		=0xCB,
	P3_PmeanTF			=0xD0,
	P3_PmeanAF,
	P3_PmeanBF,
	P3_PmeanCF,
	P3_PmeanTH,
	P3_PmeanAH,
	P3_PmeanBH,
	P3_PmeanCH			=0xD7,
	P3_IrmsN1			=0xD8, //90E32/32A/32AS invalible
	P3_UrmsA,
	P3_UrmsB,
	P3_UrmsC,
	P3_IrmsN0,
	P3_IrmsA,
	P3_IrmsB,
	P3_IrmsC,
	P3_PmeanTFLSB		=0xE0,
	P3_PmeanAFLSB,
	P3_PmeanBFLSB,
	P3_PmeanCFLSB		=0xE3,
	P3_PmeanTHLSB		=0xE4, //90E32/32A invalible
	P3_PmeanAHLSB		=0xE5, //90E32/32A invalible
	P3_PmeanBHLSB		=0xE6, //90E32/32A invalible
	P3_PmeanCHLSB		=0xE7, //90E32/32A invalible
	P3_UrmsALSB		=0xE9,
	P3_UrmsBLSB,
	P3_UrmsCLSB,
	P3_IrmsALSB		=0xED,
	P3_IrmsBLSB,
	P3_IrmsCLSB		=0xEF,
	P3_THDUA		=0xF1,
	
	P3_THDUB,
	P3_THDUC		=0xF3,
	P3_THDIA		=0xF5,
	P3_THDIB,
	P3_THDIC,
	P3_Freq,
	P3_PangleA,
	P3_PangleB,
	P3_PangleC,
	P3_Temp,
	P3_UangleA,
	P3_UangleB,
	P3_UangleC		=0xFF,
	
	P3_UPeakA		=0xF1,
	P3_IPeakA,
	P3_UPeakB,
	P3_IPeakB,
	P3_UPeakC,
	P3_IPeakC,
		
	P3_AIHR2		=0x100,	//90E36/36A
	P3_AIHR3,
	P3_AIHR4,
	P3_AIHR5,
	P3_AIHR6,
	P3_AIHR7,
	P3_AIHR8,
	P3_AIHR9,
	P3_AIHR10,
	P3_AIHR11,
	P3_AIHR12,
	P3_AIHR13,
	P3_AIHR14,
	P3_AIHR15,
	P3_AIHR16,
	P3_AIHR17,
	P3_AIHR18,
	P3_AIHR19,
	P3_AIHR20,
	P3_AIHR21,
	P3_AIHR22,
	P3_AIHR23,
	P3_AIHR24,
	P3_AIHR25,
	P3_AIHR26,
	P3_AIHR27,
	P3_AIHR28,
	P3_AIHR29,
	P3_AIHR30,
	P3_AIHR31,
	P3_AIHR32,
	P3_AITHD,
	
	P3_BIHR2,
	P3_BIHR3,
	P3_BIHR4,
	P3_BIHR5,
	P3_BIHR6,
	P3_BIHR7,
	P3_BIHR8,
	P3_BIHR9,
	P3_BIHR10,
	P3_BIHR11,
	P3_BIHR12,
	P3_BIHR13,
	P3_BIHR14,
	P3_BIHR15,
	P3_BIHR16,
	P3_BIHR17,
	P3_BIHR18,
	P3_BIHR19,
	P3_BIHR20,
	P3_BIHR21,
	P3_BIHR22,
	P3_BIHR23,
	P3_BIHR24,
	P3_BIHR25,
	P3_BIHR26,
	P3_BIHR27,
	P3_BIHR28,
	P3_BIHR29,
	P3_BIHR30,
	P3_BIHR31,
	P3_BIHR32,
	P3_BITHD,
	
	P3_CIHR2,
	P3_CIHR3,
	P3_CIHR4,
	P3_CIHR5,
	P3_CIHR6,
	P3_CIHR7,
	P3_CIHR8,
	P3_CIHR9,
	P3_CIHR10,
	P3_CIHR11,
	P3_CIHR12,
	P3_CIHR13,
	P3_CIHR14,
	P3_CIHR15,
	P3_CIHR16,
	P3_CIHR17,
	P3_CIHR18,
	P3_CIHR19,
	P3_CIHR20,
	P3_CIHR21,
	P3_CIHR22,
	P3_CIHR23,
	P3_CIHR24,
	P3_CIHR25,
	P3_CIHR26,
	P3_CIHR27,
	P3_CIHR28,
	P3_CIHR29,
	P3_CIHR30,
	P3_CIHR31,
	P3_CIHR32,
	P3_CITHD,
	
	P3_AVHR2,
	P3_AVHR3,
	P3_AVHR4,
	P3_AVHR5,
	P3_AVHR6,
	P3_AVHR7,
	P3_AVHR8,
	P3_AVHR9,
	P3_AVHR10,
	P3_AVHR11,
	P3_AVHR12,
	P3_AVHR13,
	P3_AVHR14,
	P3_AVHR15,
	P3_AVHR16,
	P3_AVHR17,
	P3_AVHR18,
	P3_AVHR19,
	P3_AVHR20,
	P3_AVHR21,
	P3_AVHR22,
	P3_AVHR23,
	P3_AVHR24,
	P3_AVHR25,
	P3_AVHR26,
	P3_AVHR27,
	P3_AVHR28,
	P3_AVHR29,
	P3_AVHR30,
	P3_AVHR31,
	P3_AVHR32,
	P3_AVTHD,
	
	P3_BVHR2,
	P3_BVHR3,
	P3_BVHR4,
	P3_BVHR5,
	P3_BVHR6,
	P3_BVHR7,
	P3_BVHR8,
	P3_BVHR9,
	P3_BVHR10,
	P3_BVHR11,
	P3_BVHR12,
	P3_BVHR13,
	P3_BVHR14,
	P3_BVHR15,
	P3_BVHR16,
	P3_BVHR17,
	P3_BVHR18,
	P3_BVHR19,
	P3_BVHR20,
	P3_BVHR21,
	P3_BVHR22,
	P3_BVHR23,
	P3_BVHR24,
	P3_BVHR25,
	P3_BVHR26,
	P3_BVHR27,
	P3_BVHR28,
	P3_BVHR29,
	P3_BVHR30,
	P3_BVHR31,
	P3_BVHR32,
	P3_BVTHD,
	
	P3_CVHR2,
	P3_CVHR3,
	P3_CVHR4,
	P3_CVHR5,
	P3_CVHR6,
	P3_CVHR7,
	P3_CVHR8,
	P3_CVHR9,
	P3_CVHR10,
	P3_CVHR11,
	P3_CVHR12,
	P3_CVHR13,
	P3_CVHR14,
	P3_CVHR15,
	P3_CVHR16,
	P3_CVHR17,
	P3_CVHR18,
	P3_CVHR19,
	P3_CVHR20,
	P3_CVHR21,
	P3_CVHR22,
	P3_CVHR23,
	P3_CVHR24,
	P3_CVHR25,
	P3_CVHR26,
	P3_CVHR27,
	P3_CVHR28,
	P3_CVHR29,
	P3_CVHR30,
	P3_CVHR31,
	P3_CVHR32,
	P3_CVTHD,
	
	P3_AIFUND,
	P3_AVFUND,
	P3_BIFUND,
	P3_BVFUND,
	P3_CIFUND,
	P3_CVFUND,
	
	P3_DFT_SCAL		=0x1D0,
	P3_DFT_CTRL		=0x1D1,
	
	P3_BGCurveK		=0x201,		//90E32AS	
	P3_BG_TEMP_P12	=0x202,
	P3_BG_TEMP_P34,
	P3_BG_TEMP_P56,
	P3_BG_TEMP_P78,
	P3_BG_TEMP_N12,
	P3_BG_TEMP_N34,
	P3_BG_TEMP_N56,
	P3_BG_TEMP_N78,	
	P3_TEMP_COMP_GAIN		=0x270,
	P3_TEMP_COMP_REF		=0x27B,
	P3_PMIGainA,
	P3_PMIGainB,
	P3_PMIGainC,
	
	
	P3_CHIPID	=0x2F0		//	
}AFE_90E3x_REG;

//=========================================================
typedef	enum
{
	AT90E2x_ChipID_Adr	=5,
	AT90E3x_ChipID_Adr	=0x2F0
}AFE_CHIPID_ADR;
typedef enum
{
	AT90E23_CHIPID		=0x2612,
    AT90E25_CHIPID      =0x2613,
	AT90E32AS_VA_CHIPID	=0x0,
	AT90E32AS_VB_CHIPID	=0x1,
	AT90E32_CHIPID		=0x1,
	AT90E32A_CHIPID		=0x3,
	AT90E36_CHIPID		=0x8001,
	AT90E36A_CHIPID		=0x8003	
}AFE_TYPE_VAL;
typedef	enum
{	
	AT90E32   =0x02,
	AT90E36	  =0x06,
	AT90E32A  =0x12,
	AT90E36A  =0x16,
	AT90E25   =0x40,
    AT90E24   =0x41,
    AT90E23   =0x42,
	AT90E32AS =0x82		
}AFE_TYPE;
typedef enum
{
	Idle_Mode =0,
	Detect_Mode,
	Patial_Mode,
	Nomal_Mode	
}AFE_MODE;

#define	INT_Num_Max		7
typedef	enum
{
	ZX0 =0,
	ZX1,
	ZX2,
	CF1,
	CF2,
	CF3,
	CF4,
//	IRQ0,
//	IRQ1	
}INT_TYPE;
#define	RMS_Num_Max		(76)
typedef enum
{
	Ua =0,
	Ub,
	Uc,
	Un,
	Ia,
	Ib,
	Ic,
	In,
	Pt,
	Pa,		//9
	Pb,
	Pc,
	Pn,
	Qt,
	Qa,
	Qb,
	Qc,
	Qn,
	SVt,	
	SAt,	//19
	Sa,
	Sb,
	Sc,
	Sn,
	PFt,
	PFa,
	PFb,
	PFc,
	PFn,
	Frq,	//29
	CNFStart,
	CALStart,
	HMStart,
	ADJStart,
	SYSStatus0,
	SYSStatus1,
	FUNCEn0,
	FUNCEn1,	//37
	Pha,		
	Phb,
	Phc,
	Phn,	
	Temperature,	//42
	UangleA,
	UangleB,
	UangleC,
	THDNUA,
	THDNUB,
	THDNUC,
	THDNIA,
	THDNIB,
	THDNIC,		//51
	
	UPeak_A,
	IPeak_A,
	UPeak_B,
	IPeak_B,
	UPeak_C,
	IPeak_C,	//56
	
	AI_HRx,
	AI_THD,
	BI_HRx,
	BI_THD,
	CI_HRx,
	CI_THD,
	AV_HRx,		//63
	AV_THD,
	BV_HRx,
	BV_THD,
	CV_HRx,
	CV_THD,
	AI_FUND,	//69
	AV_FUND,
	BI_FUND,
	BV_FUND,
	CI_FUND,	//74
	CV_FUND
		
}RMS_TYPE;

typedef enum {
	UaMeasurement,
	UbMeasurement,
	UcMeasurement
} V_RMS_TYPE;

typedef enum {
	IaMeasurement,
	IbMeasurement,
	IcMeasurement
} I_RMS_TYPE;

#endif
