/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

//
//	Includes
//
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdbool.h>
#include "spi.h"
#include "at90e_xx.h"
#include "gpio.h"

//
//	Defines
//
#define ENABLE_ASSERTS					(1)
#define ATM90_MSB_READ					(0x80)
#define ATM90_MSB_WRITE					(0x00)
#define EDISON_SPI_SSP5					(5)			// spidev5.1
#define ATM90_CS_PIN_1					(1)			// "
#define SPI_FREQ_420KHZ					(420000)
#define SPI_FREQ_200KHZ					(200000)
#define SPI_FREQ_1MHZ					(1000000)
#define SPI_FREQ_1p8MHZ					(1800000)
#define DELAY_200_MS					(200 * 1000)
#define MAGIC_REGISTER_ACCESS_NUMBER	(0x55AA)
#define MAGIC_SOFTWARE_RESET_NUMBER		(0x789A)

#define PAGE_SELECT_MASK					(0x80)
#define READ_REGISTER_MASK					(0x00)
#define WRITE_REGISTER_MASK					(0x40)
#define EDISON_SPI_SSP5						(5)			// spidev5.1
#define CS5484_CS_PIN_1						(1)			// "

// Page 0 register addresses
enum {
	regConfig0	=	0
};

//
//	Static Data
//
static spi_context spi;

//
//	Static Functions
//
static void CS5484_Init(spi_mode_t spi_mode_val);
static void CS5484_SetPage(u8 page);
static u32  CS5484_ReadRegister(u8 reg);
static void CS5484_WriteRegister(u8 reg, u32 value);
static void CS5484_TurnOnCpuClk(void);


#if ENABLE_ASSERTS
#define ASSERT(x)	assert(x)
#else
#define ASSERT(x)
#endif

/***************** MAIN ***********************/
int main(int argc, char *argv[])
{
	u32 readVal;

//	u8 hw_reg = 0;

	CS5484_Init(SPI_MODE3);

	CS5484_SetPage(0);
	while(1){
//		readVal = CS5484_ReadRegister(23);
//		printf("Reg 23 read = 0x%x\n", readVal);
//		usleep(100000);
//		readVal = CS5484_ReadRegister(24);
//		printf("Reg 24 read = 0x%x\n", readVal);
//		usleep(100000);
//		CS5484_WriteRegister(55, 0xAA);
		readVal = CS5484_ReadRegister(55);
		usleep(10000);
		//if ((readVal & 0xff) == 0xAA)
			printf("Match 0x%x\n", readVal);
//		printf("Reg 55 read = 0x%x\n", readVal);
//		usleep(100000);
	}

//	while(1){


//		readVal = CS5484_ReadRegister(hw_reg++);
//		if (hw_reg > 0x3ff) hw_reg = 0;
//		usleep(1000000);
//		usleep(100000);
//	}



	// Software reset
	//
/*
	while(1) {
		CS5484_Init(SPI_MODE0);
		CS5484_SetPage(0);
		CS5484_TurnOnCpuClk();
		CS5484_Init(SPI_MODE1);
		CS5484_SetPage(0);
		CS5484_TurnOnCpuClk();
		CS5484_Init(SPI_MODE2);
		CS5484_SetPage(0);
		CS5484_TurnOnCpuClk();
		CS5484_Init(SPI_MODE3);
		CS5484_SetPage(0);
		CS5484_TurnOnCpuClk();
		sleep(1);
	}
*/
	return 0;
}

// Init the SPI
static void CS5484_Init(spi_mode_t spi_mode_val)
{
	spi = spi_init_raw(EDISON_SPI_SSP5, CS5484_CS_PIN_1);
	spi_frequency(spi, SPI_FREQ_420KHZ);
	spi_mode(spi, spi_mode_val);
}

static void CS5484_TurnOnCpuClk(void)
{
	u32 writeVal = 0x400000 + (1<<20); // Default + CUCLK_ON
	CS5484_WriteRegister(regConfig0, writeVal);
}

static void CS5484_SetPage(u8 page)
{
	uint8_t txbuf[] =
	{
		page + PAGE_SELECT_MASK,
	};
	uint8_t rxbuf[] =
	{
		0x00,
	};

	spi_transfer_buf(spi, txbuf, rxbuf, 1);
}


static u32 CS5484_ReadRegister(u8 reg)
{
	u32 returnVal;
	u8 txbuf[] =
	{
			reg | READ_REGISTER_MASK,
			0,
			0,
			0
	};
	u8 rxbuf[] =
	{
			0x00,
			0x00,	// msb
			0x00,	// middle
			0x00	// lsb
	};

	spi_transfer_buf(spi, txbuf, rxbuf, 4);

	returnVal = ((u32) rxbuf[3] << 16) +
				((u32) rxbuf[2] << 8)  +
				((u32) rxbuf[1]);

	return returnVal;
}

static void CS5484_WriteRegister(u8 reg, u32 value)
{

	u8 msb = (value & 0xff0000) >> 16;
	u8 second = (value & 0x00ff00) >> 8;
	u8 lsb = (value & 0xff);

	uint8_t txbuf[] =
	{
			reg | WRITE_REGISTER_MASK,
			msb,
			second,
			lsb
	};
	uint8_t rxbuf[] =
	{
			0x00,
			0x00,
			0x00,
			0x00
	};

	spi_transfer_buf(spi, txbuf, rxbuf, 4);
/*
	printf("Register write\n");
	printf("txbuf[0] = 0x%x\n", txbuf[0]);
	printf("txbuf[1] = 0x%x\n", txbuf[1]);
	printf("txbuf[2] = 0x%x\n", txbuf[2]);
	printf("txbuf[3] = 0x%x\n\n", txbuf[3]);
*/
}
















