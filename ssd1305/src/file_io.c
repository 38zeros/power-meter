/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include "types.h"
#include <string.h>

#include <stdio.h>
int read_file (s32* parameters)
{
   static const char filename[] = "/var/volatile/lcd_powerdata.txt";
   FILE *file = fopen ( filename, "r" );
   int j, i = 0;

   if ( file != NULL )
   {
      char line [ 128 ]; /* or other suitable maximum line size */
      char *start_of_number;
      while ( fgets ( line, sizeof line, file ) != NULL ) /* read a line */
      {
    	for (j = 0 ; j < sizeof line ; j++)
    	{
    		if (line[j] == ':')
    		{
    			start_of_number = line + j + 1;
    			break;
    		}
    	}

         parameters[i++] = atoi(start_of_number);
      }
      fclose ( file );
      return 1;
   }
   else
   {
//      perror ( filename ); /* why didn't the file open? */
      return 0;
   }
}

int read_buffer(u8* value)
{
   static const char filename[] = "/var/volatile/lcd_buffer.txt";
   FILE *file = fopen ( filename, "r" );

   if ( file != NULL )
   {
      char line [ 128 ]; /* or other suitable maximum line size */
      fgets ( line, sizeof line, file );
      *value = atoi(line);
      return 1;
   }
   else
   {
	   return 0;
   }
}

int read_status (char* status_string)
{
   static const char filename[] = "/var/volatile/lcd_status.txt";
   FILE *file = fopen ( filename, "r" );

   if ( file != NULL )
   {
      char line [ 128 ]; /* or other suitable maximum line size */
      fgets ( line, sizeof line, file );
      memcpy(status_string, line, sizeof line);
      return 1;
   }
   else
   {
	   return 0;
   }
}

