/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/fcntl.h>
#include <stdlib.h>
#include "types.h"
#include "server_socket.h"
#include "ssd1309.h"

#define exit(x) return
#define CMD_PORT 3000


static int listen_fd = -1, incoming_fd = -1;
char ok[] = "OK";

CommandData const cmdData[] =
{
		{ "lcd.set.status", cmdSTATUS	},
		{ "lcd.set.bars",	cmdBARS		},
		{ "lcd.set.buffer",	cmdBUFFER	},

		{ "lcd.set.volts1",	cmdVOLTS_1	},
		{ "lcd.set.volts2",	cmdVOLTS_2	},
		{ "lcd.set.volts3",	cmdVOLTS_3	},
		{ "lcd.set.voltsT",	cmdVOLTS_T	},

		{ "lcd.set.amps1",	cmdAMPS_1	},
		{ "lcd.set.amps2",	cmdAMPS_2	},
		{ "lcd.set.amps3",	cmdAMPS_3	},
		{ "lcd.set.ampsT",	cmdAMPS_T	},

		{ "lcd.set.watts1",	cmdWATTS_1	},
		{ "lcd.set.watts2",	cmdWATTS_2	},
		{ "lcd.set.watts3",	cmdWATTS_3	},
		{ "lcd.set.wattsT",	cmdWATTS_T	},

		{ "lcd.set.vars1",	cmdVARS_1	},
		{ "lcd.set.vars2",	cmdVARS_2	},
		{ "lcd.set.vars3",	cmdVARS_3	},
		{ "lcd.set.varsT",	cmdVARS_T	},
};
const int commandSz = sizeof(cmdData)/sizeof(CommandData);

static void handle_command(Command cmd, char* value);
static void parse(char* input, char* command, char* str_value);

void init_listen_socket(void)
{
  int sock = -1;
  struct sockaddr_in server_addr;
  int reuse = 1; /* enable SO_REUSEADDR on socket */
  int status;

  sock = socket(AF_INET, SOCK_STREAM, 0);
  status = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

  memset(&server_addr, 0, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  server_addr.sin_port = htons(CMD_PORT);

  status = bind(sock, (struct sockaddr *)&server_addr, sizeof(server_addr));
  if (status < 0)
  {
    fprintf(stderr, "Failed to bind to port %d\n", CMD_PORT);
    exit(1);
  }

  if (listen(sock, 1) < 0)
  {
    fprintf(stderr, "Failed to listen on port %d\n", CMD_PORT);
    exit(1);
  }

	if (fcntl(sock, F_SETFL, O_NONBLOCK) < 0) {
		perror("Can't set socket to non-blocking");
		exit(0);
	}

	listen_fd = sock;
}

void non_blocking_accept(void)
{
	struct sockaddr_in incoming_connection;
	unsigned int incoming_connection_len = sizeof(incoming_connection);
	int new_fd;

	new_fd = accept(listen_fd,
			(struct sockaddr *)&incoming_connection,
			&incoming_connection_len);

	if (new_fd != -1) {
//		printf("New connection from port %d\r\n",ntohs(incoming_connection.sin_addr.s_addr));
		close(incoming_fd);
		incoming_fd = new_fd;
		if (fcntl(incoming_fd, F_SETFL, O_NONBLOCK) < 0) {
			perror("Can't set accepted socket to non-blocking");
			exit(0);
		}
	}
}

void non_blocking_read(void)
{
	char recvBuff[40], command[40], value[40];
	int n, i;

	if (incoming_fd != -1)
	{
		n = read(incoming_fd, recvBuff, sizeof(recvBuff) - 1);
		if (n > 0)
		{
			//printf("recvBuff %s\n", recvBuff);
			parse(recvBuff, command, value);
			for (i = 0; i < commandSz; i++)
			{
				if (!strcmp(command, cmdData[i].command_text))
				{
					handle_command(cmdData[i].cmd, value);
					break;
				}
			}
		}
	}
}

static void handle_command(Command cmd, char* str_value)
{
	u16 int_value = atoi(str_value);

	switch(cmd) {

	case cmdSTATUS:
//		printf("cmdSTATUS value = %s\n", str_value);
		update_status(str_value);
		break;

	case cmdBARS:
//		printf("cmdBARS value = %d\n", int_value);
		update_bars(int_value);
		break;
	case cmdBUFFER:
//		printf("cmdBUFFER value = %d\n", int_value);
		update_buffer(int_value);
		break;

	case cmdVOLTS_1:
	case cmdVOLTS_2:
	case cmdVOLTS_3:
	case cmdVOLTS_T:
	case cmdAMPS_1:
	case cmdAMPS_2:
	case cmdAMPS_3:
	case cmdAMPS_T:
	case cmdWATTS_1:
	case cmdWATTS_2:
	case cmdWATTS_3:
	case cmdWATTS_T:
	case cmdVARS_1:
	case cmdVARS_2:
	case cmdVARS_3:
	case cmdVARS_T:
		update_measurement(cmd, int_value);
		break;

	default:
		printf("Unhandled command: %d\n", cmd);
		break;
	}
}

// Example
// input lcd.set.bars 4
// command is lcd.set.bars
// value is 4
// Split input into command and value (if any)
static void parse(char* input, char* command, char* value)
{
	// scan chars until either a space or \n is reached
	// If \n is reached, return command and value is null
	// if space is reached, command is chars up till then, keep scanning
	// once \n is reached, these characters are the value string.
	u8 i, j;

	command[0] = 0;
	value[0] = 0;

	for ( i = 0 ; i < 40 ; i++ )
	{
		if (input[i] == '\n')
		{
			value[0] = 0;
			memcpy(command, input, i);
			command[i] = 0;	// String terminator

			return;
		}
		if (input[i] == ' ')
		{
			memcpy(command, input, i);
			command[i] = 0;	// String terminator
			break;
		}
	}
	j = i;

	for ( i = j;  i < 40 ; i++ )
	{
		if (input[i] == '\n')
		{
			memcpy(value, input+j + 1, i);
			value[i-j-1] = 0;	// String terminator
			return;
		}
	}
	printf("Error in parse()");
}
