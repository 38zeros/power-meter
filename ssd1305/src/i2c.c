/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include "types.h"
#include "i2c.h"

//
//	Defines
//
#define ENABLE_ASSERTS					(1)
#define FILENAME_SZ						(20)
#define EDISON_I2C_BUS					1
#define SSD1305_ADDR					(0x3d)
#define CONTINUATION_DATA_ONLY_MASK		(0x00)
#define CONTINUATION_NORMAL_MASK		(0x80)

enum {
	CMD_ENTIRE_DISP_ON = 0xA5,
	CMD_DISPLAY_ON = 0xAF,
	CMD_DISPLAY_OFF = 0xAE,
};

//
//	Static Data
//
static uint32_t file;

//
//	Static Functions
//


#if ENABLE_ASSERTS
#define ASSERT(x)	assert(x)
#else
#define ASSERT(x)
#endif


void i2c_open_device(void)
{
	  char filename[FILENAME_SZ];

	  // Open i2c device
	  snprintf(filename, FILENAME_SZ-1, "/dev/i2c-%d", EDISON_I2C_BUS);
	  file = open(filename, O_RDWR);
	  ASSERT(file >= 0);

	  // Set i2c address
	  if (ioctl(file, I2C_SLAVE, SSD1305_ADDR) < 0) {
		  ASSERT(0);
	  }
}

void i2c_turn_all_pixels_on(void)
{
	write_data_single(CONTINUATION_NORMAL_MASK, CMD_ENTIRE_DISP_ON);
	write_data_single(CONTINUATION_NORMAL_MASK, CMD_DISPLAY_ON);
}

void i2c_turn_all_pixels_off(void)
{
	write_data_single(CONTINUATION_NORMAL_MASK, CMD_ENTIRE_DISP_ON);
	write_data_single(CONTINUATION_NORMAL_MASK, CMD_DISPLAY_OFF);
}

void write_data_single(u8 control_byte, u8 data_byte)
{
	int32_t retVal;

	retVal = i2c_smbus_write_byte_data(file, control_byte, data_byte);
	if (retVal != -1) {
		return;
	}
	printf("write_data_single failed\n");
}
