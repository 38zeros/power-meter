/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#ifndef GPIO_H_
#define GPIO_H_
#include "types.h"
#include <stdbool.h>

typedef enum {
	GPIO_MODE_0,	// This is generally the 'gpio mode'
	GPIO_MODE_1,	// This is generally the alternate mode like I2C
} GpioMode;

typedef enum {
	GPIO128_LCD_A0 			= 128,
	GPIO129_LCD_NRESET 		= 129,
	GPIO19_I2C_1_SCL 		= 19,
	GPIO20_I2C_1_SDA 		= 20,
	GPIO27_I2C_6_SCL 		= 27,
	GPIO28_I2C_6_SDA 		= 28,
} GpioPin;

typedef enum {
	DIRECTION_NA,
	DIRECTION_INPUT,
	DIRECTION_OUTPUT
} GpioDirection;

typedef enum {
	GPIO_LOW	= 0,
	GPIO_HIGH	= 1,
	GPIO_NA,
} GpioValue;

typedef struct {
	GpioPin pin;
	GpioMode mode;
	GpioDirection direction;
	GpioValue initial_value;
} GpioDesc;

void gpio_init_gpio(void);
void gpio_export(GpioPin gpio);
void gpio_set_direction(GpioPin gpio, GpioDirection direction);
void gpio_set(GpioPin gpio, GpioValue value);
bool gpio_get(GpioPin gpio);

#endif /* GPIO_H_ */
