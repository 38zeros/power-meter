/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdbool.h>

#include "i2c.h"
#include "u8g.h"
#include "u8g_edison.h"


static void draw(void);
/*========================================================================*/
/* main */

u8g_t u8g;

void u8g_prepare(void) {
  //u8g_SetFont(&u8g, u8g_font_6x10);
  u8g_SetFont(&u8g, u8g_font_5x7);
  u8g_SetFontRefHeightExtendedText(&u8g);
  u8g_SetDefaultForegroundColor(&u8g);
  u8g_SetFontPosTop(&u8g);
}

void u8g_box_frame(uint8_t a) {
  u8g_DrawStr(&u8g, 0, 0, "drawBox");
  u8g_DrawBox(&u8g, 5,10,20,10);
  u8g_DrawBox(&u8g, 10+a,15,30,7);
  u8g_DrawStr(&u8g, 0, 30, "drawFrame");
  u8g_DrawFrame(&u8g, 5,10+30,20,10);
  u8g_DrawFrame(&u8g, 10+a,15+30,30,7);
}

void u8g_string(uint8_t a) {
  u8g_DrawStr(&u8g, 30+a,31, " 0");
  u8g_DrawStr90(&u8g, 30,31+a, " 90");
  u8g_DrawStr180(&u8g, 30-a,31, " 180");
  u8g_DrawStr270(&u8g, 30,31-a, " 270");
}

void u8g_line(uint8_t a) {
  u8g_DrawStr(&u8g, 0, 0, "drawLine");
  u8g_DrawLine(&u8g, 7+a, 10, 40, 55);
  u8g_DrawLine(&u8g, 7+a*2, 10, 60, 55);
  u8g_DrawLine(&u8g, 7+a*3, 10, 80, 55);
  u8g_DrawLine(&u8g, 7+a*4, 10, 100, 55);
}

void u8g_ascii_1(void) {
  char s[2] = " ";
  uint8_t x, y;
  u8g_DrawStr(&u8g, 0, 0, "u8g_font_4x6");
  for( y = 0; y < 6; y++ ) {
    for( x = 0; x < 16; x++ ) {
      s[0] = y*16 + x + 32;
      u8g_DrawStr(&u8g, x*7, y*10+10, s);
    }
  }
}

void u8g_ascii_2(void) {
  char s[2] = " ";
  uint8_t x, y;
  u8g_DrawStr(&u8g, 0, 0, "ASCII page 2");
  for( y = 0; y < 6; y++ ) {
    for( x = 0; x < 16; x++ ) {
      s[0] = y*16 + x + 160;
      u8g_DrawStr(&u8g, x*7, y*10+10, s);
    }
  }
}

void u8g_ascii_lines(void) {
  u8g_DrawStr(&u8g, 0, 0, "u8g_font_5x7 Line");
  u8g_DrawStr(&u8g, 0,  ((&u8g)->line_spacing), "u8g_font_5x7 Line");
  u8g_DrawStr(&u8g, 0, 2* ((&u8g)->line_spacing), "u8g_font_5x7 Line");
  u8g_DrawStr(&u8g, 0, 3* ((&u8g)->line_spacing), "u8g_font_5x7 Line");
  u8g_DrawStr(&u8g, 0, 4* ((&u8g)->line_spacing), "u8g_font_5x7 Line");
  u8g_DrawStr(&u8g, 0, 5* ((&u8g)->line_spacing), "u8g_font_5x7 Line");
  u8g_DrawStr(&u8g, 0, 6* ((&u8g)->line_spacing), "u8g_font_5x7 Line");
  u8g_DrawStr(&u8g, 0, 7* ((&u8g)->line_spacing), "u8g_font_5x7 Line");


  u8g_DrawStr(&u8g, 0, 7* u8g_GetFontLineSpacing(&u8g), "u8g_font_5x7 Line");

}

void u8g_ascii_lines_90(void)
{
  uint8_t i;

  for ( i = 0 ; i < 128 /((&u8g)->line_spacing); i++ )
  {
	  u8g_DrawStr90(&u8g, 128-(i)*((&u8g)->line_spacing), 0, "ABCDEFGHIJKL");
  }
}



uint8_t draw_state = 0;

static void draw(void)
{
  u8g_prepare();
  switch(draw_state >> 3) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4: u8g_ascii_lines_90(); break;
   // case 4: u8g_ascii_1(); break;
//    case 0: u8g_box_frame(draw_state&7); break;
//   case 1: u8g_string(draw_state&7); break;
//    case 2: u8g_line(draw_state&7); break;
//    case 3: u8g_ascii_1(); break;
//    case 4: u8g_ascii_2(); break;
  }
}




void graphics_test(void)
{
  /*
    Please uncomment one of the displays below
    Notes:
      - "2x", "4x": high speed version, which uses more RAM
      - "hw_spi": All hardware SPI devices can be used with software SPI also.
	Access type is defined by u8g_com_hw_spi_fn
  */
 	u8g_InitComFn(&u8g, &u8g_dev_ssd1309_128x64_i2c, u8g_com_i2c_fn);

  /* flip screen, if required */
  //u8g_SetRot180(&u8g);
  

  //for(;;)
  //{
    /* picture loop */
    u8g_FirstPage(&u8g);
    draw();


    do
    {
      draw();
    } while ( u8g_NextPage(&u8g) );

    /*
    draw_state++;
    if ( draw_state >= 5*8 )
      draw_state = 0;
    */
    /* refresh screen after some delay */
    
    u8g_Delay(100);
    
  //}
}

