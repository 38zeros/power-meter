/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include "types.h"
#include "telit.h"

#define N_GSM0710	21	/* GSM 0710 Mux */
#define DEFAULT_SPEED	B115200
#define SERIAL_PORT	"/dev/ttyACM0"

#define DEBUG

#ifdef	DEBUG
#define	debugf(fmt,args...)	printf (fmt ,##args)
#else
#define debugf(fmt,args...)
#endif

static 	int fd;
static char buf[256];

void telit_open(void)
{
	//int fd;
	struct termios old_tio;
	struct termios tio;

	/* open the serial port connected to the modem */
	fd = open(SERIAL_PORT, O_RDWR | O_NOCTTY | O_NDELAY);

	tcgetattr(fd, &old_tio);
	ioctl(fd, TIOCSETD, 0);
	memset(&tio, 0, sizeof(tio));
	/* configure the serial port : speed, flow control ... */

	tio.c_iflag = 0;
	tio.c_oflag = 0;
	tio.c_cflag = CS8 | CREAD | CLOCAL; // 8n1, see termios.h for more information
	tio.c_lflag = 0;
	tio.c_cc[VMIN] = 1;
	tio.c_cc[VTIME] = 0;
	cfsetospeed(&tio, B115200);            // 115200 baud
	cfsetispeed(&tio, B115200);            // 115200 baud
	tcsetattr(fd, TCSANOW, &tio);
	tcflush(fd, TCIOFLUSH);
	write(fd, "ATZ\r", 4);
	usleep(50000);
	memset(buf, 0, 128);
	read(fd, buf, 256);
	//debugf("ATZ: %s\n", buf);

}

void telit_close(void)
{
    tcflush(fd, TCIOFLUSH);
    close(fd);
}

u8 telit_get_signal_quality(void)
{
	int total_n = 0;
	int n;
	int i;

	write(fd, "AT+CSQ\r", 7);
	usleep(50000);
	memset(buf, 0, 128);
	read(fd, buf, 256);

	while (1 == sscanf(buf + total_n, "%*[^0123456789]%d%n", &i, &n))
	{
	    total_n += n;
	    break;
	}

	return (u8) i;
}

u8 get_number_of_bars(void)
{
	u8 value;
	u8 ret_value;
	// Open serial connection to Telit via USB
	telit_open();
	value = telit_get_signal_quality();
	telit_close();
//	printf("teli %d\n", value);
	if (value > 98)
	{
		return 5;
	}
	else if (value > 15) ret_value = 4;
	else if (value > 10) ret_value = 3;
	else if (value > 6)	 ret_value = 2;
	else if (value > 1) ret_value = 1;
	else {
		printf("Got unexpected CSQ: %d\r\n", value);
		ret_value = 0;
	}
	return ret_value;
}


