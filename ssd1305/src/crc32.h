/*
 * crc32.h
 *
 *  Created on: Jun 14, 2015
 *      Author: robbaltzer
 */

#ifndef CRC32_H_
#define CRC32_H_
#include "types.h"


unsigned long
crc32(unsigned long crc, const void *buf, unsigned long size);

#endif /* CRC32_H_ */
