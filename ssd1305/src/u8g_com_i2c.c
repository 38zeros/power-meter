/*
  
  u8g_com_i2c.c

  generic i2c interface

  Universal 8bit Graphics Library
  
  Copyright (c) 2011, olikraus@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, 
  are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this list 
    of conditions and the following disclaimer.
    
  * Redistributions in binary form must reproduce the above copyright notice, this 
    list of conditions and the following disclaimer in the documentation and/or other 
    materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
  
*/

#define U8G_RASPBERRY_PI
#define U8G_EDISON
#include "u8g.h"

//#define U8G_I2C_WITH_NO_ACK

static uint8_t u8g_i2c_err_code;
static uint8_t u8g_i2c_opt;		/* U8G_I2C_OPT_NO_ACK, SAM: U8G_I2C_OPT_DEV_1 */
/*
  position values
    1: start condition
    2: sla transfer
*/
static uint8_t u8g_i2c_err_pos;


void u8g_i2c_clear_error(void)
{
  u8g_i2c_err_code = U8G_I2C_ERR_NONE;
  u8g_i2c_err_pos = 0;
}

uint8_t  u8g_i2c_get_error(void)
{
  return u8g_i2c_err_code;
}

uint8_t u8g_i2c_get_err_pos(void)
{
  return u8g_i2c_err_pos;
}

static void u8g_i2c_set_error(uint8_t code, uint8_t pos)
{
  if ( u8g_i2c_err_code > 0 )
    return;
  u8g_i2c_err_code |= code;
  u8g_i2c_err_pos = pos;
}

#include "i2c.h"
#include "gpio.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#define I2C_SLA         0x3c

static uint8_t i2cMode = 0;

void u8g_i2c_init(void)
{
	u8g_i2c_clear_error();
	i2c_open_device();
	gpio_init_gpio();
}

uint8_t u8g_i2c_start(uint8_t sla)
{
   u8g_i2c_send_mode(0);

   return 1;
}

void u8g_i2c_stop(void)
{
}

uint8_t u8g_i2c_send_mode(uint8_t mode)
{
   i2cMode = mode;
   return 0;
} 

uint8_t u8g_i2c_send_byte(uint8_t data)
{

	write_data_single(i2cMode, data);
	//printf("u8g_i2c_send_byte mode 0x%x data 0x%x\n", i2cMode, data);

//   wiringPiI2CWriteReg8(fd, i2cMode, data);
	/* i2c function */
   return 1;
}

uint8_t u8g_i2c_wait(uint8_t mask, uint8_t pos)
{
  return 1;
}



