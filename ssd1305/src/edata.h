/*
 * edata.h
 *
 *  Created on: Jun 13, 2015
 *      Author: robbaltzer
 */

#ifndef EDATA_H_
#define EDATA_H_
// EData packet it constructed in three sections:

#include "types.h"

// NOTES:
/*
 * Edata header file header information (for seachability if needed later)

[09:28] Zac Wheeler: Example first-line data from xxd:
0000000: 6564 6174 6100 001e c008 f056 100e 0000  edata......V...

The first six bytes are "edata"  as a null-terminated string (6564 6174 6100)

That example is for a meter w/ ID 001EC008F056 (001e c008 f056) for 3600 seconds of data (100e 0000)

This only needs to be added once to the top of the .edata file.

We should name the edata file:
001EC008F056-YYYY-MM-DD-HH.edata and gzip it before sending
Final filename will be e.g. 001EC008F056-2012-07-17-00.edata.gz

// Edata per-second header size:
           //
           //        2 - Packet Size (11 bytes + numberofpackets*sizeof(packets))
           //        4 - UTC time (seconds since epoch)
           //        1 - SequenceEncoding (not used)
           //        1 - Channels (bitfield � 0x0f is four)
           //        1 - NumberOfSamples (nominally 60 for 60Hz or 50 for 50Hz (as in India (Bosch))
           //        2 - Folded CRC-32

zacwheeler [9:35 PM]
number of samples is numberOfWirePackets/2 because it is the number of AC cycles that you�ve recorded

zacwheeler [9:37 PM]
so you will have 120 wirepackets for a 60 hz signal

zacwheeler [9:39 PM]
for the crc: UINT16 crc_folded = (crc ^ (crc >> 16)) & 0xffff;

Note crc is actually footer � sent after wirepackets/wiresamples

0000000: 6564 6174 6100 001e c008 f056 100e 0000  edata......V....
          e d  a t  a   --------ID---- --3600s--

0000010: 6b09 00ab 0450 000f 3cb7 0000 1900 00c1  k....P..<.......
		 -PS- ---T S--- SQCH NSP1      Q1     I1
PS=Packet Size
TS=Time Stamp
SQ=Sequence (not used)
CH=Channels
NS=SamplesPerSecond (0x3c = 60Hz)

0000020: 0066 2f95 0000 0500 0096 00f1 2f00 0000  .f/........./...
           V1 V1P2      Q2     I2   V2 V2

0000030: 0000 0000 0000 0000 0000 0000 0000 0000  ................


0000040: 00b0 0000 1900 00ba 0060 2f8f 0000 0600  .........`/.....
           P1      Q1

0000050: 008f 00e9 2f00 0000 0000 0000 0000 0000  ..../...........
0000060: 0000 0000 0000 0000 00ae 0000 1900 00b8  ................
0000070: 0066 2f8e 0000 0600 008e 00ec 2f00 0000  .f/........./...


 40 bytes repeating, 10 bytes per wiresample*4
 */

/*
 * type WireSample struct {
	P1AVG_A int16
	P1AVG_B int8
	Q1AVG_A int16
	Q1AVG_B int8
	I1RMS   uint16
	V1RMS   uint16
	P2AVG_A int16
	P2AVG_B int8
	Q2AVG_A int16
	Q2AVG_B int8
	I2RMS   uint16
	V2RMS   uint16
}
 */
/*
 * DEVICE-ID: 001EC008F056 device
NumberOfSecondRecords: 3600
2012-Jul-17 00:00:00-00: (183W,25var,1.93A,121.34V) (149W,5var,1.50A,122.73V) (0W,0var,0.00A,0.00V) (0W,0var,0.00A,0.00V)
2012-Jul-17 00:00:00-01: (176W,25var,1.86A,121.28V) (143W,6var,1.43A,122.65V) (0W,0var,0.00A,0.00V) (0W,0var,0.00A,0.00V)
2012-Jul-17 00:00:00-02: (174W,25var,1.84A,121.34V) (142W,6var,1.42A,122.68V) (0W,0var,0.00A,0.00V) (0W,0var,0.00A,0.00V)
2012-Jul-17 00:00:00-03: (181W,26var,1.91A,121.34V) (144W,6var,1.44A,122.71V) (0W,0var,0.00A,0.00V) (0W,0var,0.00A,0.00V)
2012-Jul-17 00:00:00-04: (176W,25var,1.86A,121.29V) (144W,6var,1.43A,122.69V) (0W,0var,0.00A,0.00V) (0W,0var,0.00A,0.00V)
 */

//0000000: 6564 6174 6100 001e c008 f056 100e 0000  edata......V....
//          e d  a t  a   --------ID---- --3600s--
//
//0000010: 6b09 00ab 0450 000f 3cb7 0000 1900 00c1  k....P..<.......
// 		   -PS- ---T S--- SQCH NSP1      Q1     I1

// PS=Packet Size
// TS=Time Stamp
// SQ=Sequence (not used)
// CH=Channels
// NS=SamplesPerSecond (0x3c = 60Hz)

#define NUMBER_OF_CHANNELS	(4)	// Two Cirrus chips @ 2 phases each

// Only one of these headers at the beginning of the edata packet
typedef struct
{
	u64 meter_id;			// e.g. 001EC008F056 (6 bytes)
	u32 wirepackets;		// The number of seconds of data (wirepackets) in this edata packet
} EDATA_PACKET_HEADER_T;

typedef struct
{
	u16 wirepacket_size;		// Size of per second packet 11* bytes + samples_per_sec*sizeof(WIRESAMPLE_T)) = 11+(60*40) = 2411
	u32 timestamp;				// Timestamp expressed as seconds since epoch
	u8  channels;				// Bitfield of which channels are being used. 4 channels would be 0b00001111b = 0x0f
	u8  samples_per_sec;		// Samples per second (either 50 or 60) Bosch will be 50 since India runs on 50 Hz.
}WIREPACKET_HEADER_T;

// There are two phases per WIRESAMPLE to match the two phases available
// in the Cirrus chip used at the time. These measurements are made every A/C cycle.
typedef struct
{
	u32 p1;	// Power Average on Phase 1 (24 bits)
	u32 q1; // VARs Average on Phase 1	(24 bits)
	u16 i1; // Current RMS on Phase 1
	u16 v1;	// Volts RMS on Phase 1

	u32 p2;	// Power Average on Phase 2	(24 bits)
	u32 q2; // VARs Average on Phase 2 (24 bits)
	u16 i2; // Current RMS on Phase 2
	u16 v2;	// Volts RMS on Phase 2
} CHIPSAMPLE_T;

// Each wiresample contains samples from two chips
typedef struct
{
	CHIPSAMPLE_T chip1;
	CHIPSAMPLE_T chip2;
} WIRESAMPLE_T;

// A wirepacket is one seconds worth of samples
typedef struct
{
	WIREPACKET_HEADER_T header;		// One wirepacket header
	WIRESAMPLE_T* sample;			// Multiple wiresamples
	u16 crc_folded;					// crc added to the end
} WIREPACKET_T;

// Each edata packet is a header followed by n wirepackets
typedef struct
{
	EDATA_PACKET_HEADER_T header;
	WIREPACKET_T* wirepacket;
} EDATA_PACKET_T;

void edata2bytestream(EDATA_PACKET_T* edata, u8* bytestream);

#endif /* EDATA_H_ */
