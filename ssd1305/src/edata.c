/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <stdio.h>
#include <linux/types.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <sys/time.h>
#include <endian.h>
#include "edata.h"
#include "types.h"
#include "crc32.h"


#define WIREPKT_SZ
#define BYTES_PER_SAMPLE	(10)	// 3 bytes for P&Q plus 2 bytes each for I&V for total of 10 bytes
#define NUMBER_OF_CHIPS		(2)
#define CHANNELS_PER_CHIP	(2)
#define TOTAL_SAMPLE_BYTES	(BYTES_PER_SAMPLE*NUMBER_OF_CHIPS*CHANNELS_PER_CHIP)
//
//	Convert edata packet into a bytestream that can be put transmitted to the server
//
void edata2bytestream(EDATA_PACKET_T* edata, u8* bytestream)
{
	FILE *edata_file = NULL;
	int i = 0, j;
	const char start_string[] = "edata";
	u64 temp_meter_id;
    u32 crc;
    u16 crc_folded;

	memset(&bytestream[0], 0xAA, 50000);	// TODO: Remove once things are working

	// Create edata packet header
	memcpy(&bytestream[0], start_string, strlen(start_string)+1);	// Copy in 'edata' plus the null terminator (hence the +1)
	temp_meter_id = htobe64(edata->header.meter_id)>>16;			// Endianess of meter_id is different than all the other elements
	memcpy(&bytestream[6], &temp_meter_id, 6);						// meter_id is only 6 bytes, not 8 bytes afforded by u64 datatype
	memcpy(&bytestream[12], &edata->header.wirepackets, sizeof(edata->header.wirepackets));

	// Create wirepacket header
	for (i = 0 ; i < edata->header.wirepackets ; i++)
	{
		// Generate wirepacket header
		memcpy(&bytestream[16 + (i*edata->wirepacket[i].header.wirepacket_size)],
				&edata->wirepacket[i].header.wirepacket_size, 2);
		memcpy(&bytestream[18 + (i*edata->wirepacket[i].header.wirepacket_size)],
				&edata->wirepacket[i].header.timestamp, 4);
		bytestream[22 + (i*edata->wirepacket[i].header.wirepacket_size)] = 0;			// Sequence byte is not used, set to zero
		bytestream[23 + (i*edata->wirepacket[i].header.wirepacket_size)] = edata->wirepacket[i].header.channels;
		bytestream[24 + (i*edata->wirepacket[i].header.wirepacket_size)] = edata->wirepacket[i].header.samples_per_sec;

		// Add wiresamples
		for (j = 0 ; j < edata->wirepacket[0].header.samples_per_sec ; j++)
		{
			// Samples from chip #1
			memcpy(&bytestream[25 + (j*TOTAL_SAMPLE_BYTES) + (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip1.p1, 3);	// Copy 3 bytes since q1 is 24 bits (not 32)
			memcpy(&bytestream[28 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip1.q1, 3);	// Copy 3 bytes since q1 is 24 bits (not 32)
			memcpy(&bytestream[31 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip1.i1, 2);
			memcpy(&bytestream[33 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip1.v1, 2);
			memcpy(&bytestream[35 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip1.p2, 3);	// Copy 3 bytes since q1 is 24 bits (not 32)
			memcpy(&bytestream[38 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip1.q2, 3);	// Copy 3 bytes since q1 is 24 bits (not 32)
			memcpy(&bytestream[41 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip1.i2, 2);
			memcpy(&bytestream[43 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip1.v2, 2);

			// Samples from chip #2
			memcpy(&bytestream[45 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip2.p1, 3);	// Copy 3 bytes since q1 is 24 bits (not 32)
			memcpy(&bytestream[48 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip2.q1, 3);	// Copy 3 bytes since q1 is 24 bits (not 32)
			memcpy(&bytestream[51 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip2.i1, 2);
			memcpy(&bytestream[53 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip2.v1, 2);
			memcpy(&bytestream[55 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip2.p2, 3);	// Copy 3 bytes since q1 is 24 bits (not 32)
			memcpy(&bytestream[58 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip2.q2, 3);	// Copy 3 bytes since q1 is 24 bits (not 32)
			memcpy(&bytestream[61 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip2.i2, 2);
			memcpy(&bytestream[63 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)],
					&edata->wirepacket[i].sample[j].chip2.v2, 2);
		}

		crc = 0;
		crc = crc32(crc,
					&bytestream[16 + (i*edata->wirepacket[i].header.wirepacket_size)],
					edata->wirepacket[i].header.wirepacket_size);
		crc_folded = (crc ^ (crc >> 16)) & 0xffff;
		memcpy(&bytestream[65 + (j*TOTAL_SAMPLE_BYTES)+ (i*edata->wirepacket[i].header.wirepacket_size)], &crc_folded, 2);
	}


	edata_file = fopen("edata_file.edata", "w");
	fwrite(bytestream,
			16 + (edata->header.wirepackets *
			edata->wirepacket[0].header.wirepacket_size),
			1,
			edata_file);
//	  ret = fwrite(data_buffer, FRAME_BYTES, 1, temp_file);
	fclose(edata_file);


}


