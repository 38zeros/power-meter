#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdbool.h>
#include "u8g.h"

#define I2C_SLA		0x3c
#define I2C_CMD_MODE	0x000
#define I2C_DATA_MODE	0x040

#if defined(U8G_WITH_PINLIST)
uint8_t u8g_com_edison_ssd_start_sequence(u8g_t *u8g)
{
	/* are we requested to set the a0 state? */
	if (u8g->pin_list[U8G_PI_SET_A0] == 0)
	{
		return 1;
	}

	/* setup bus, might be a repeated start */
	if (u8g_i2c_start(I2C_SLA) == 0)
	{
		return 0;
	}
#if 0
	if (u8g->pin_list[U8G_PI_A0_STATE] == 0)
	{
		if (u8g_i2c_send_mode(I2C_CMD_MODE) == 0)
		{
			return 0;
		}
	}
	else
	{
		if (u8g_i2c_send_mode(I2C_DATA_MODE) == 0)
		{
			return 0;
		}
	}
#endif

	if (u8g->pin_list[U8G_PI_A0_STATE] == 0)
	{
		u8g_i2c_send_mode(I2C_CMD_MODE);
	}
	else
	{
		u8g_i2c_send_mode(I2C_DATA_MODE);
	}

	u8g->pin_list[U8G_PI_SET_A0] = 0;
	return 1;
}

//uint8_t u8g_com_raspberrypi_ssd_i2c_fn(u8g_t *u8g, uint8_t msg, uint8_t arg_val, void *arg_ptr)
uint8_t u8g_com_i2c_fn(u8g_t *u8g, uint8_t msg, uint8_t arg_val, void *arg_ptr)
{
  switch(msg)
  {
    case U8G_COM_MSG_INIT:
      u8g_i2c_init();
      //u8g_SetPIOutput(u8g, U8G_PI_RESET);
      //u8g_SetPIOutput(u8g, U8G_PI_A0);
      break;

    case U8G_COM_MSG_STOP:
      break;

    case U8G_COM_MSG_RESET:
      break;

	case U8G_COM_MSG_CHIP_SELECT:
		u8g->pin_list[U8G_PI_A0_STATE] = 0;
		u8g->pin_list[U8G_PI_SET_A0] = 1; /* force a0 to set again, also forces start condition */
		if (arg_val == 0)
		{
			/* disable chip, send stop condition */
			u8g_i2c_stop();
		} else
		{
			/* enable, do nothing: any byte writing will trigger the i2c start */
		}
		break;

    case U8G_COM_MSG_WRITE_BYTE:
      //u8g->pin_list[U8G_PI_SET_A0] = 1;
      if ( u8g_com_edison_ssd_start_sequence(u8g) == 0 )
    	  return u8g_i2c_stop(), 0;
      if ( u8g_i2c_send_byte(arg_val) == 0 )
    	  return u8g_i2c_stop(), 0;
      // u8g_i2c_stop();
      break;

	case U8G_COM_MSG_WRITE_SEQ:
		//u8g->pin_list[U8G_PI_SET_A0] = 1;
		if (u8g_com_edison_ssd_start_sequence(u8g) == 0)
			return u8g_i2c_stop(), 0;
		{
			register uint8_t *ptr = (uint8_t *) arg_ptr;
			while (arg_val > 0) {
				if (u8g_i2c_send_byte(*ptr++) == 0)
					return u8g_i2c_stop(), 0;
				arg_val--;
			}
		}
		// u8g_i2c_stop();
		break;

    case U8G_COM_MSG_WRITE_SEQ_P:
      //u8g->pin_list[U8G_PI_SET_A0] = 1;
      if ( u8g_com_edison_ssd_start_sequence(u8g) == 0 )
	return u8g_i2c_stop(), 0;
      {
        register uint8_t *ptr = (uint8_t *)arg_ptr;
        while( arg_val > 0 )
        {
	  if ( u8g_i2c_send_byte(u8g_pgm_read(ptr)) == 0 )
	    return 0;
          ptr++;
          arg_val--;
        }
      }
      // u8g_i2c_stop();
      break;

    case U8G_COM_MSG_ADDRESS:                     /* define cmd (arg_val = 0) or data mode (arg_val = 1) */
      u8g->pin_list[U8G_PI_A0_STATE] = arg_val;
      u8g->pin_list[U8G_PI_SET_A0] = 1;		/* force a0 to set again */

      break;
  }
  return 1;
}

#else	/* defined(U8G_WITH_PINLIST) */

uint8_t u8g_com_raspberrypi_ssd_i2c_fn(u8g_t *u8g, uint8_t msg, uint8_t arg_val, void *arg_ptr) {
   return 1;
}

#endif	/* defined(U8G_WITH_PINLIST) */

