/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdbool.h>

#include "i2c.h"
#include "u8g.h"
#include "u8g_edison.h"

/*========================================================================*/
/* main */

u8g_t u8g;

void draw(uint8_t pos)
{
//  u8g_SetFont(&u8g, u8g_font_unifont);
  u8g_DrawStr(&u8g,  0, 12+pos, "Hello World!");
}

void hello_world(void) {
	uint8_t pos = 0;
	/*
	 Please uncomment one of the displays below
	 Notes:
	 - "2x", "4x": high speed version, which uses more RAM
	 - "hw_spi": All hardware SPI devices can be used with software SPI also.
	 Access type is defined by u8g_com_hw_spi_fn
	 */

	u8g_InitComFn(&u8g, &u8g_dev_ssd1309_128x64_i2c, u8g_com_i2c_fn);
//   u8g_InitComFn(&u8g, &u8g_dev_ssd1309_128x64_hw_spi, u8g_com_hw_spi_fn);

	u8g_SetDefaultForegroundColor(&u8g);
	  u8g_SetFont(&u8g, u8g_font_unifont);

	for (;;) {
		/* picture loop */
		u8g_FirstPage(&u8g);
		do {
			draw(pos);
		} while (u8g_NextPage(&u8g));

		/* refresh screen after some delay */
		u8g_Delay(100);
		usleep(1000*500);

		/* update position */
		pos++;
		pos &= 15;
	}
}
