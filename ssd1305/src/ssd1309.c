/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

//
//	Includes
//
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include "i2c.h"
#include "hello_world.h"
#include "gpio.h"
#include "telit.h"
#include "graphics_test.h"
#include "u8g.h"
#include "u8g_edison.h"
#include "server_socket.h"
#include "edata.h"
#include "file_io.h"

//
//	Defines
//
#define ENABLE_ASSERTS					(1)
#define LCD_WIDTH 						(128)
#define LCD_HEIGHT 						(64)
#define LCD_PAGE_HEIGHT 				(8)
#define HORIZ_BAR_SPACING				(7)
#define BAR_HEIGHT						(8)
#define BAR_WIDTH						(6)
#define FONT_WIDTH						(6)
#define FONT_HIEGHT						(10)
#define NO_PHASES						(3)

#define LINE_SPACING()					u8g_GetFontLineSpacing(&u8g)

//
//  Typedefs
//
typedef struct {
	char* title;
	s32 value[4];
} PARAMETERS;



typedef enum {
	VALUE_PHASE1	= 0,
	VALUE_PHASE2,
	VALUE_PHASE3,
	VALUE_TOTAL
} VALUE_ID;

static char* parameter_unit[4] =
{
		"  VOLTS",
		"  AMPS",
		"  WATTS",
		"  VARS"
};

//static char* connection_status[2] =
//{
//		" Connected",
//		" Searching"
//};

//
//	Static Data
//
static u8g_t u8g;
static char m_status[128] = "Connected";
static char m_hostname[64] = "Unknown";
static u8 m_bars = 2;
static u8 m_buffer = 0;
static s32 m_measurements[16];
static s32 m_raw_measurements[12];

static u8 cylon_pos = 0;
static u8 cylon_dir = 0;


//
//	Static Functions
//
static void draw_bars(u8 bars);
static void draw_hostname(char* hostname);
static void draw_connected_status(char* status_string);
static void draw_buffer_percent(u8 buffer);
static void draw_entire_screen(u8 debug_dot, u8 bars, u8 buffer, char* connect_status, PARAMETERS parms, char* hostname);
static void draw_parameters(PARAMETERS parms);

#if ENABLE_ASSERTS
#define ASSERT(x)	assert(x)
#else
#define ASSERT(x)
#endif

/***************** MAIN ***********************/
int main(int argc, char *argv[])
{
	u8 debug_dot = 0;
	u8 signal = 0;
	PARAMETERS parms;

	char status[128] = "Connected";
	char hostname[64] = "Unknown";
	u8 buffer, bars = 0;
	int i = 0;

#if 0
	int j;
	EDATA_PACKET_T edata;
	u8 bytestream[50000];
	WIREPACKET_T wirepacket[3600];
	WIRESAMPLE_T sample[60];
#endif
//	printf("1234 TEST ssd1309\n");

#if 0	// edata packet code
	edata.header.meter_id = 0x001EC008F056ULL;
	edata.header.wirepackets = 300; // 300 seconds equals 5 minutes
	for ( j = 0 ; j < edata.header.wirepackets ; j++)
	{
		wirepacket[j].header.channels = 0xf;
		wirepacket[j].header.timestamp = 1342483200 + j; // TODO: Derive from local time
		wirepacket[j].header.samples_per_sec = 60;
		wirepacket[j].header.wirepacket_size = 2411;
		wirepacket[j].sample = sample;

		for (i = 0 ; i < 60 ; i ++)
		{
			wirepacket[j].sample[i].chip1.p1 = 183;
			wirepacket[j].sample[i].chip1.q1 = 25;
			wirepacket[j].sample[i].chip1.i1 = 193;
			wirepacket[j].sample[i].chip1.v1 = 12134;
			wirepacket[j].sample[i].chip1.p2 = 182;
			wirepacket[j].sample[i].chip1.q2 = 25;
			wirepacket[j].sample[i].chip1.i2 = 193;
			wirepacket[j].sample[i].chip1.v2 = 12134;

			wirepacket[j].sample[i].chip2.p1 = 183;
			wirepacket[j].sample[i].chip2.q1 = 25;
			wirepacket[j].sample[i].chip2.i1 = 193;
			wirepacket[j].sample[i].chip2.v1 = 12134;
			wirepacket[j].sample[i].chip2.p2 = 0;
			wirepacket[j].sample[i].chip2.q2 = 0;
			wirepacket[j].sample[i].chip2.i2 = 0;
			wirepacket[j].sample[i].chip2.v2 = 0;
		}
	}

	edata.wirepacket = &wirepacket[0];

	edata2bytestream(&edata, bytestream);

	return 1;
#endif
	memset(m_measurements, 16*sizeof(u16), 0);

	// Open I2C port to LCD
	i2c_open_device();

	// Open serial connection to Telit via USB
	telit_open();

	// Init u8glib library
	u8g_InitComFn(&u8g, &u8g_dev_ssd1309_128x64_i2c, u8g_com_i2c_fn);
	u8g_SetFont(&u8g, u8g_font_6x10);
	u8g_SetFontRefHeightExtendedText(&u8g);
	u8g_SetDefaultForegroundColor(&u8g);
	u8g_SetFontPosTop(&u8g);

	while(1)
	{
		const int k_no_seconds = 2;

		if (read_file(m_raw_measurements)) {
			m_measurements[0] = m_raw_measurements[0];
			m_measurements[1] = m_raw_measurements[1];
			m_measurements[2] = m_raw_measurements[2];
			m_measurements[3] = m_raw_measurements[0] + m_raw_measurements[1] + m_raw_measurements[2];

			m_measurements[4] = m_raw_measurements[3];
			m_measurements[5] = m_raw_measurements[4];
			m_measurements[6] = m_raw_measurements[5];
			m_measurements[7] = m_raw_measurements[3]+ m_raw_measurements[4] + m_raw_measurements[5];

			m_measurements[8] = m_raw_measurements[6];
			m_measurements[9] = m_raw_measurements[7];
			m_measurements[10] = m_raw_measurements[8];
			m_measurements[11] = m_raw_measurements[6]+ m_raw_measurements[7] + m_raw_measurements[8];

			m_measurements[12] = m_raw_measurements[9];
			m_measurements[13] = m_raw_measurements[10];
			m_measurements[14] = m_raw_measurements[11];
			m_measurements[15] = m_raw_measurements[9]+ m_raw_measurements[10] + m_raw_measurements[11];
		}

		if (gethostname(hostname, sizeof(hostname)) == 0) {
			memcpy(m_hostname, hostname, sizeof(hostname));
		}

		m_bars = get_number_of_bars();
//		m_bars = 5;
		if (read_buffer(&buffer))
		{
			m_buffer = buffer;
		}

		if (read_status(status))
		{
			memcpy(m_status, status, sizeof status);
		}

		// delay
		usleep(1000 * 1000 * k_no_seconds);

		if (bars++ > 4) bars = 0;
		if (signal++ > 100) signal = 0;
		if (i++ >= 3) i = 0;
		parms.title = parameter_unit[i];
		parms.value[0] = m_measurements[i*4];
		parms.value[1] = m_measurements[1 + 4*i];
		parms.value[2] = m_measurements[2 + 4*i];
		parms.value[3] = m_measurements[3 + 4*i];

		// Picture loop
		u8g_FirstPage(&u8g);
		do
		{
			draw_entire_screen(debug_dot, m_bars, m_buffer, m_status, parms, m_hostname);
//			draw_entire_screen(debug_dot, bars, m_buffer, m_status, parms);

		}
		while (u8g_NextPage(&u8g));

		// Update blinking debug dot (may be taken out in final product)
		debug_dot ^= 1;

		if (cylon_dir == 0)
		{
			cylon_pos = cylon_pos + 1;
			if (cylon_pos == 3) cylon_dir = 1;
		}
		else
		{
			cylon_pos = cylon_pos - 1;
			if (cylon_pos == 0) cylon_dir = 0;
		}


	}

	return 0;
}


void update_status(char* status)
{
	memcpy(m_status, status, strlen(status));
}


void update_bars(u8 bars)
{
	m_bars = bars;
}

void update_buffer(u8 buffer)
{
	m_buffer = buffer;
}

void update_measurement(Command index, u16 value)
{
	m_measurements[index] = value;
}

static void draw_entire_screen(u8 debug_dot, u8 bars, u8 buffer, char* connect_status, PARAMETERS parms, char* hostname)
{
	u8 line = 0;

	// Needed in case debug dot turned pixel color off
	u8g_SetColorIndex(&u8g, 1);

#if NON_ROTATED
	// Static
	u8g_DrawStr(&u8g, 0, 0 * u8g_GetFontLineSpacing(&u8g), "  BOSCH POWER METER");
	u8g_DrawStr(&u8g, 0, 2 * (u8g_GetFontLineSpacing(&u8g)+2), "   STATUS : ");
	u8g_DrawStr(&u8g, 0, 3 * (u8g_GetFontLineSpacing(&u8g)+2), "   SIGNAL : ");
	u8g_DrawStr(&u8g, 0, 4 * (u8g_GetFontLineSpacing(&u8g)+2), "  BUFFER :      ");
#else
	u8g_DrawStr90(&u8g, LCD_WIDTH - line++ * LINE_SPACING(), 2, " PHANTOM");
	line++;	// Space for dynamic data
	u8g_DrawStr90(&u8g, LCD_WIDTH - line++ * LINE_SPACING(), 0, "  STATUS");
	line++; // Space for dynamic data
	u8g_DrawStr90(&u8g, LCD_WIDTH - line++ * LINE_SPACING(), 0, "  SIGNAL");
	line++; // Space for dynamic data
	u8g_DrawStr90(&u8g, LCD_WIDTH - line++ * LINE_SPACING(), 0, "  BUFFER");

#endif

	// Dynamic
	draw_hostname(hostname);
	draw_connected_status(connect_status);
	draw_bars(bars);
	draw_buffer_percent(buffer);

	draw_parameters(parms);
	u8g_SetColorIndex(&u8g, debug_dot);				// Draw debug dot
	u8g_DrawPixel(&u8g, 0, LCD_HEIGHT-1);			// "
}



// Draw 5x7 signal strength bars (0-4).
// The bars will start with their upper left corner pixel (x, y).
static void draw_bars(u8 bars)
{

#if NON_ROTATED
	u8 i;
	u8 x = FONT_WIDTH * 11;
	u8 y = 3 * (u8g_GetFontLineSpacing(&u8g) + 2);

	for (i = 0 ; i < 4 ; i ++)
	{
		if (bars > i)
		{
			u8g_DrawBox(&u8g, x + (i*HORIZ_BAR_SPACING), y, BAR_WIDTH, BAR_HEIGHT);
		}
		else
		{
			u8g_DrawFrame(&u8g, x + (i*HORIZ_BAR_SPACING), y, BAR_WIDTH, BAR_HEIGHT);
		}
	}
#else
	u8 i;
	u8 x = 8 * LINE_SPACING() + 4;
	u8 y = FONT_WIDTH * 3;


	if (bars < 5)
	{
		for (i = 0 ; i < 4 ; i ++)
		{
			if (bars > i)
			{
				u8g_DrawBox(&u8g, x , y + (i*HORIZ_BAR_SPACING), BAR_HEIGHT-(3-i), BAR_WIDTH);
			}
			else
			{
				u8g_DrawFrame(&u8g, x, y + (i*HORIZ_BAR_SPACING), BAR_HEIGHT-(3-i), BAR_WIDTH);
			}
		}
	}
	else
	{
		// we are in cyclon raider mode


		for (i = 0 ; i < 4 ; i ++)
		{
			if (cylon_pos == i)
			{
				u8g_DrawBox(&u8g, x , y + (i*HORIZ_BAR_SPACING), BAR_HEIGHT-(3-i), BAR_WIDTH);
			}
			else
			{
				u8g_DrawFrame(&u8g, x, y + (i*HORIZ_BAR_SPACING), BAR_HEIGHT-(3-i), BAR_WIDTH);
			}


		}

	}
#endif
}


// Update the hostname.
// The text will be start with the upper left pixel at (x, y).

static void draw_hostname(char* hostname)
{
	if (hostname[0] == '3' && hostname[1] == '8' && (hostname[2] == 'z' || hostname[2] == 'Z') && hostname[3] == '-') {
		hostname += 4; // don't show extraneous characters
	}
#if NON_ROTATED

	u8 x = (FONT_WIDTH * 11);
	u8 y = 1 * (u8g_GetFontLineSpacing(&u8g)+2);
#else
	u8 x = LCD_WIDTH - 1 * LINE_SPACING();
	u8 y = FONT_WIDTH * 2;
#endif

	u8g_DrawStr90(&u8g, x,y, hostname);
}


// Update the connected status.
// The text will be start with the upper left pixel at (x, y).

static void draw_connected_status(char* status_string)
{
#if NON_ROTATED

	u8 x = (FONT_WIDTH * 11);
	u8 y = 2 * (u8g_GetFontLineSpacing(&u8g)+2);
#else
	u8 x = LCD_WIDTH - 3 * LINE_SPACING();
	u8 y = 0;
#endif

	u8g_DrawStr90(&u8g, x,y, status_string);
}


// Draw the amount of buffer space left.
// The text will be start with the upper left pixel at (x, y).
static void draw_buffer_percent(u8 buffer)
{
#if NON_ROTATED
	char str[80];
	u8 x = (FONT_WIDTH * 11);
	u8 y = 4 * (u8g_GetFontLineSpacing(&u8g)+2);

	sprintf(str, "%d %%", buffer);
	u8g_DrawStr(&u8g, x, y, str);
#else
	char str[80];
	u8 x = 7 * LINE_SPACING() + 2;
	u8 y = FONT_WIDTH * 4;

	sprintf(str, "%d %%", buffer);
	u8g_DrawStr90(&u8g, x, y, str);
#endif
}

// Draw the parameters. Values are fixed point (xxxx.x). In other words, all values are divided by 10.
static void draw_parameters(PARAMETERS parms)
{
	u8 tmp_line = 8;
	char tmp_str[80];
	VALUE_ID i;
	char volts[] = "  VOLTS";
	char blank[] = "             ";

	u8g_DrawStr90(&u8g, LCD_WIDTH - tmp_line++ * LINE_SPACING(), 0, parms.title);

	for (i = VALUE_PHASE1 ; i < VALUE_TOTAL; i++)
	{
		sprintf(tmp_str, "%d: %6.1f", (i+1), (double) ((double) parms.value[i]/10.0F));
		u8g_DrawStr90(&u8g, LCD_WIDTH - tmp_line++ * LINE_SPACING(), 0, tmp_str);
	}

	if (!strcmp (volts, parms.title))
	{
		u8g_DrawStr90(&u8g, LCD_WIDTH - tmp_line++ * LINE_SPACING(), 0, blank);
	}
	else {
		sprintf(tmp_str, "T: %6.1f", (double) ((double) parms.value[VALUE_TOTAL]/10.0F));
		u8g_DrawStr90(&u8g, LCD_WIDTH - tmp_line++ * LINE_SPACING(), 0, tmp_str);
	}
}
