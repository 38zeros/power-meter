/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <stdio.h>
#include <linux/types.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <sys/time.h>
#include "types.h"
#include "gpio.h"

//#define BOARD_MODDED	(1)	// Uncomment if your board was modded to run I2C on I2C_6 instead of I2C_1

// Digital GPIO table
static GpioDesc gpioDesc[] = {
//		   GPIO					Mode				Direction		    Initial Value
		{ GPIO128_LCD_A0, 		GPIO_MODE_0,		DIRECTION_OUTPUT, 		GPIO_HIGH},
		{ GPIO129_LCD_NRESET,	GPIO_MODE_0,		DIRECTION_OUTPUT, 		GPIO_HIGH},
#ifndef BOARD_MODDED
		{ GPIO19_I2C_1_SCL,		GPIO_MODE_1,		DIRECTION_NA, 			GPIO_NA},
		{ GPIO20_I2C_1_SDA,		GPIO_MODE_1,		DIRECTION_NA, 			GPIO_NA},
#else
		{ GPIO27_I2C_6_SCL,		GPIO_MODE_1,		DIRECTION_NA, 			GPIO_NA},
		{ GPIO28_I2C_6_SDA,		GPIO_MODE_1,		DIRECTION_NA, 			GPIO_NA},
#endif
};
static const u8 gpioDescSz = sizeof(gpioDesc)/sizeof(GpioDesc);

// Initialize all of the digital gpio
void gpio_init_gpio(void)
{
	u8 i;

	for (i = 0 ; i < gpioDescSz ; i++) {
		gpio_export(gpioDesc[i].pin);
		gpio_set_direction(gpioDesc[i].pin, gpioDesc[i].direction);
		if (gpioDesc[i].direction == DIRECTION_OUTPUT) {
			gpio_set(gpioDesc[i].pin, gpioDesc[i].initial_value);
		}
	}
}

//echo mode0 > /sys/kernel/debug/gpio_debug/gpio28/current_pinmux

void gpio_set_mode(GpioPin gpio, GpioMode mode)
{
	int fd;
	char buf[128];

	sprintf(buf, "/sys/kernel/debug/gpio_debug/gpio%d/current_pinmux", gpio);
	fd = open(buf, O_WRONLY);
	if (mode == GPIO_MODE_0)
	{
		write(fd, "mode0", 5);
	}
	else if (mode == GPIO_MODE_1)
	{
		write(fd, "mode1", 5);
	}
	else {
		// Do nothing
	}
	close(fd);
}

void gpio_export(GpioPin gpio)
{
	int fd;
	char buf[128];

	fd = open("/sys/class/gpio/export", O_WRONLY);
	sprintf(buf, "%d", gpio);
	write(fd, buf, strlen(buf));
	close(fd);
}

void gpio_set_direction(GpioPin gpio, GpioDirection direction)
{
	int fd;
	char buf[128];

	sprintf(buf, "/sys/class/gpio/gpio%d/direction", gpio);
	fd = open(buf, O_WRONLY);
	if (direction == DIRECTION_OUTPUT) {
		write(fd, "out", 3);
	}
	else if (direction == DIRECTION_INPUT) {
		write(fd, "in", 2);
	}
	else {
		// Do nothing
	}
	close(fd);
}

void gpio_set(GpioPin gpio, GpioValue value)
{
	int fd;
	char buf[128];

	sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
	fd = open(buf, O_WRONLY);
	if (value == GPIO_LOW) {
		write(fd, "0", 1);
	}
	else if (value == GPIO_HIGH){
		write(fd, "1", 1);
	}
	else {
		// Do nothing
	}
	close(fd);
}

bool gpio_get(GpioPin gpio)
{
	int fd;
	char buf[128];
	u8 value;

	sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
	fd = open(buf, O_RDONLY);
	read(fd, &value, 1);
	close(fd);
	return (value == '0' ? false : true);
}
