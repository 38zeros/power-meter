/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/fcntl.h>
#include <stdlib.h>
#include "types.h"
#include "client_socket.h"
#include "network.h"

#define exit(x) return
#define CMD_PORT 3000


//static int listen_fd = -1, incoming_fd = -1;
char ok[] = "OK";

CommandData const cmdData[] =
{
		{ "lcd.set.status", cmdSTATUS		},
		{ "lcd.set.bars",	cmdBARS			},
		{ "lcd.set.buffer",	cmdBUFFER		},

		{ "lcd.set.volts1",	cmdVOLTS_1		},
		{ "lcd.set.volts2",	cmdVOLTS_2		},
		{ "lcd.set.volts3",	cmdVOLTS_3		},
		{ "lcd.set.voltsT",	cmdVOLTS_T		},

		{ "lcd.set.amps1",	cmdAMPS_1		},
		{ "lcd.set.amps2",	cmdAMPS_2		},
		{ "lcd.set.amps3",	cmdAMPS_3		},
		{ "lcd.set.ampsT",	cmdAMPS_T		},

		{ "lcd.set.watts1",	cmdWATTS_1		},
		{ "lcd.set.watts2",	cmdWATTS_2		},
		{ "lcd.set.watts3",	cmdWATTS_3		},
		{ "lcd.set.wattsT",	cmdWATTS_T		},

		{ "lcd.set.vars1",	cmdVARS_1		},
		{ "lcd.set.vars2",	cmdVARS_2		},
		{ "lcd.set.vars3",	cmdVARS_3		},
		{ "lcd.set.varsT",	cmdVARS_T		},
};
const int commandSz = sizeof(cmdData)/sizeof(CommandData);

int sockfd = 0 /*,n = 0*/;

void init_client_socket(void)
{
    struct sockaddr_in serv_addr;

    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Error : Could not create socket \n");
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(3000);

    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
    }

    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       printf("\n Error : Connect Failed \n");
    }
}

void client_write_socket(void)
{
    int i = 2;
    char buf[40];

	sprintf(buf, "lcd.set.bars %d\n", i);
    i = 10;
//    while (i--)
//    {
//    	write(sockfd, "lcd.set.status rqearching\n", 26);
//    	usleep(1000*10);
//    }
    write(sockfd, buf, 15);
}
