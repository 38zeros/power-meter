/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

//
//	Includes
//
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/i2c-dev-user.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/time.h>
#include <unistd.h>
#include "gpio.h"
#include "telit.h"
#include "client_socket.h"

//
//	Defines
//


//
//  Typedefs
//




//
//	Static Data
//


//
//	Static Functions
//


#if ENABLE_ASSERTS
#define ASSERT(x)	assert(x)
#else
#define ASSERT(x)
#endif

/***************** MAIN ***********************/
int main(int argc, char *argv[])
{
	init_client_socket();

	client_write_socket();

	return 0;
}
