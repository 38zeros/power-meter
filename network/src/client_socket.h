/*
 * Copyright 2015 by 38 Zeros, Portland, Oregon.
 *
 *
 * All Rights Reserved
 *
 *
 * This file may not be modified, copied, or distributed in part or in whole
 * without prior written consent from 38 Zeros.
 *
 *
 * 38 ZEROS DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * 38 ZEROS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#ifndef __SOCKET_H___
#define __SOCKET_H___

typedef enum {
	cmdVOLTS_1 = 0,
	cmdVOLTS_2,
	cmdVOLTS_3,
	cmdVOLTS_T,

	cmdAMPS_1,
	cmdAMPS_2,
	cmdAMPS_3,
	cmdAMPS_T,

	cmdWATTS_1,
	cmdWATTS_2,
	cmdWATTS_3,
	cmdWATTS_T,

	cmdVARS_1,
	cmdVARS_2,
	cmdVARS_3,
	cmdVARS_T,

	cmdSTATUS,
	cmdBARS,
	cmdBUFFER,

} Command;

typedef struct {
	char command_text[40];
	Command cmd;
} CommandData;

void init_listen_socket(void);
void non_blocking_read(void);
void non_blocking_accept(void);

void client_write_socket(void);
void init_client_socket(void);
#endif
